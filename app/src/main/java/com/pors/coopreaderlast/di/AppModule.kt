package com.pors.coopreaderlast.di

import android.app.Application
import androidx.room.Room
import com.pors.coopreaderlast.api.SybaseApi
import com.pors.coopreaderlast.data.SybaseDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl(SybaseApi.BASE_URL)
            .client(provideOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
//    @Provides
//    @Singleton
//    fun provideRestaurantApi(retrofit: Retrofit): RestaurantApi =
//        retrofit.create(RestaurantApi::class.java)

    @Provides
    @Singleton
    fun provideSybaseApi(retrofit: Retrofit): SybaseApi =
        retrofit.create(SybaseApi::class.java)

    @Provides
    @Singleton
    fun provideDatabase(app: Application): SybaseDatabase =
        Room.databaseBuilder(app, SybaseDatabase::class.java, "horovice")
            .fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build()

    @Provides
    @Singleton
    fun provideOkHttpClient() =
        OkHttpClient.Builder()
            .readTimeout(6000, TimeUnit.SECONDS)
            .callTimeout(6000, TimeUnit.SECONDS)
            .build()
}