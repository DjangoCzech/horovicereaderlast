package com.pors.coopreaderlast.api

import android.content.Context
import com.pors.coopreaderlast.data.*
import com.pors.coopreaderlast.features.edit.EditActivity
import retrofit2.Response
import retrofit2.http.*

interface SybaseApi {

    companion object {
        //Dasice
        const val BASE_URL = "http://10.0.1.28:5000/"

        //const val BASE_URL = "http://192.168.1.25:5000/"
        //const val BASE_URL = "http://192.168.3.118:5000/"

        //Horovice
       //const val BASE_URL = "http://192.168.1.39:5000/"
    }
    @GET("getSkladnik")
    suspend fun getSkladnik(): List<Skladnik>

    @GET("getHlavicky")
    suspend fun getHlavicky(): List<Hlavicka>

    @GET("getDoklad")
    suspend fun getPolozky(): List<Polozka>

    @GET("getZbozi")
    suspend fun getZbozi(): List<Zbozi>

    @GET("getProdejna")
    suspend fun getProdejna(): List<Prodejna>

    @POST("posts")
    suspend fun pushPost(
        @Body post: SkladnikTest
    ): Response<SkladnikTest>

    @FormUrlEncoded
    @POST("posts")
    suspend fun pushPost2(
        @Field("prac") prac: String,
        @Field("heslo") heslo: String,
        @Field("zebra") zebra: String
    ): Response<SkladnikTest>

    @FormUrlEncoded
    @POST("updStavPolozka")
    suspend fun updStavPolozka(
        @Field("doklad") doklad: String,
        @Field("exp") exp: String
    ): Response<HlavickaPost>
}


