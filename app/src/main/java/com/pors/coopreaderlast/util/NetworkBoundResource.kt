package com.pors.coopreaderlast.util

import kotlinx.coroutines.flow.*

inline fun <ResultType, RequestType> networkBoundResource(
    //data from database
    crossinline query: () -> Flow<ResultType>,
     //new data from API
    crossinline fetch: suspend () -> RequestType,
     //data from API ulozi do DTB
    crossinline saveFetchResult: suspend (RequestType) -> Unit,
     //
    crossinline shouldFetch: (ResultType) -> Boolean = { true }
) = flow {
    val data = query().first()

    val flow = if (shouldFetch(data)){
        emit(Resource.Loading(data))
        //vlozi data do dtb
        try {
            saveFetchResult(fetch())
            query().map { Resource.Success(it) }
        } catch (throwable: Throwable){
            query().map { Resource.Error(throwable, it) }
        }
    } else {
        query().map { Resource.Success(it) }
    }
    emitAll(flow)
}