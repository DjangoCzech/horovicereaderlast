package com.pors.coopreaderlast.util

import java.net.ServerSocket

class JUnit4ExampleTest {
    val port = findRandomPort()
    var url = "http://localhost:$port"

    //@Rule
    //@JvmField


    fun findRandomPort(): Int {
        ServerSocket(0).use { socket -> return socket.localPort }

    }
}