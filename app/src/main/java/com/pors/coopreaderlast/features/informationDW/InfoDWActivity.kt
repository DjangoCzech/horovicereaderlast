package com.pors.coopreaderlast.features.informationDW

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.activity.viewModels
import com.pors.coopreaderlast.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_info.*
import kotlinx.android.synthetic.main.activity_info_dwactivity.*
import java.io.IOException

@AndroidEntryPoint
class InfoDWActivity : AppCompatActivity() {
    private val viewModel : InfoActivityDWViewModel by viewModels()
    private val statusTextView by lazy { findViewById<TextView>(R.id.textNaz_AI_DW) }
    private val btnSkenIni by lazy { findViewById<Button>(R.id.btnSken_DW) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info_dwactivity)
        textNaz_AI_DW.text = "Načtěte čárový kód"

        DWUtilities.CreateDWProfile(this)
            }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        val result = displayScanResult(intent)

        if (result != null){
            try {
                val countRegEan = viewModel.getCountRegByEAN(result)
                if (countRegEan == 0){
                    AlertDialog.Builder(this)
                        .setTitle("EAN!")
                        .setMessage("Tento EAN není zadán v číselníku kódů")
                        .setPositiveButton(
                            android.R.string.ok,
                            DialogInterface.OnClickListener { dialog, which ->
                            }) // A null listener allows the button to dismiss the dialog and take no further action.

                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show()
                } else {
                    statusTextView.text = result.toString()
                    val resultEan = viewModel.infoByEan(result)
                    tv_EAN_AI_DW.text = "EAN: ${resultEan.cak}"
                    textReg_AI_DW.text = "REG: ${resultEan.reg}"
                    textVeb_AI_DW.text = "VEB: ${resultEan.veb.toString()}"
                    textNaz_AI_DW.text = "Název: ${resultEan.naz}"
                    textZasoba_AI_DW.text = "Zásoba: ${resultEan.zasoba}"
                    textZnacky_AI_DW.text = "Místo: ${resultEan.znacky}"
                }
            }catch (e: IOException){
                statusTextView.text = "Problem s nactenim EAN kodu"
            }
        } else {
            statusTextView.text = "Nepodarilo se nacist EAN. Provedte nastaveni v DataWedge"
        }

    }

    private fun displayScanResult(scanIntent: Intent):String? {
        val decodeSource =
            scanIntent.getStringExtra(resources.getString(R.string.datawedge_intent_key_source))
        val decodeData =
            scanIntent.getStringExtra("com.symbol.datawedge.data_string")
        val scan: String? = decodeData
        return scan
        //val output = findViewById<TextView>(R.id.tv_EAN_AI_DW)
        Log.d("scan", "Zaciname skenovat")
        //output.text = scan
        Log.d("scan", "Koncime skenovat: $scan")
        //return scan
    }


}