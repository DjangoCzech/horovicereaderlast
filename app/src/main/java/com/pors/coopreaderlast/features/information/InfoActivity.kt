@file:Suppress("DEPRECATION")

package com.pors.coopreaderlast.features.information

import android.app.AlertDialog
import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.os.AsyncTask
import android.os.Build
import android.util.Log
import android.widget.Button
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import com.pors.coopreaderlast.R

import com.symbol.emdk.EMDKManager
import com.symbol.emdk.EMDKManager.EMDKListener
import com.symbol.emdk.EMDKManager.FEATURE_TYPE
import com.symbol.emdk.EMDKResults
import com.symbol.emdk.barcode.*
import com.symbol.emdk.barcode.BarcodeManager.ConnectionState
import com.symbol.emdk.barcode.BarcodeManager.ScannerConnectionListener
import com.symbol.emdk.barcode.Scanner.DataListener
import com.symbol.emdk.barcode.Scanner.StatusListener


import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_info.*
import kotlinx.android.synthetic.main.activity_zebra.*
import java.io.IOException
import java.lang.Exception

@AndroidEntryPoint
//
class InfoActivity : AppCompatActivity(), EMDKListener, StatusListener, DataListener, ScannerConnectionListener {
    private var emdkManager: EMDKManager? = null
    private var barcodeManager: BarcodeManager? = null
    private var scanner: Scanner? = null
    private val viewModel : InfoActivityViewModel by viewModels()
    private val eanTextView by lazy {findViewById<TextView>(R.id.tv_EAN_AI)}
    private val statusTextView by lazy { findViewById<TextView>(R.id.textNaz_AI) }
    private val btnSkenIni by lazy { findViewById<Button>(R.id.btnSken) }
    private var deviceList: List<ScannerInfo>? = null
    private var scannerIndex = 0 // Keep the selected scanner
    private var defaultIndex = 0 // Keep the default scanner
    private var triggerIndex = 0
    private var dataLength = 0
    private var statusString = ""
    //private var deviceList: List<ScannerInfo>? = null

    private val triggerStrings = arrayOf("HARD", "SOFT")
    class Items(val items: List<String>)

    //@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        deviceList = ArrayList()
        try {
            val results = EMDKManager.getEMDKManager(applicationContext, this)
            if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS){
                statusTextView!!.text = "Status: " + "Problem s EMDKManazerem"
                return
            }
        } catch (e: Exception){
            statusTextView.text = e.message
        }
        textNaz_AI.text = "Načtěte čárový kód"

        if (android.os.Build.MANUFACTURER.contains("Zebra Technologies")){
            Log.d("manuf", Build.MANUFACTURER.toString())
            btnSkenIni.visibility = Button.INVISIBLE
        } else {
            btnSkenIni.visibility = Button.VISIBLE
        }

        //addSpinnerScannerDevicesListener()
        //enumerateScannerDevices()
        //setTrigger()
        //setDecoders()


    //viewModel.infoByEan(ean)

    }
    private inner class AsyncDataUpdate : AsyncTask<String, Void, String>(){
        override fun doInBackground(vararg params: String): String {
            return params[0]
        }

        override fun onPostExecute(result: String?) {
            if (result != null){
                try {
                    val countRegEan = viewModel.getCountRegByEAN(result)
                    if (countRegEan == 0){
                        AlertDialog.Builder(this@InfoActivity)
                            .setTitle("EAN!")
                            .setMessage("Tento EAN není zadán v číselníku kódů")
                            .setPositiveButton(
                                android.R.string.ok,
                                DialogInterface.OnClickListener { dialog, which ->
                                }) // A null listener allows the button to dismiss the dialog and take no further action.

                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show()
                    } else {
                        statusTextView.text = result.toString()
                        val resultEan = viewModel.infoByEan(result)
                        tv_EAN_AI.text = "EAN: ${resultEan.cak}"
                        textReg_AI.text = "REG: ${resultEan.reg}"
                        textVeb_AI.text = "VEB: ${resultEan.veb.toString()}"
                        textNaz_AI.text = "Název: ${resultEan.naz}"
                        textZasoba_AI.text = "Zásoba: ${resultEan.zasoba}"
                        textZnacky_AI.text = "Místo: ${resultEan.znacky}"
                    }

                }catch (e: IOException){
                    statusTextView.text = "Problem s nactenim EAN kodu"
                }
            }
        }
    }

    override fun onOpened(emdkManager: EMDKManager) {
        this.emdkManager = emdkManager
        barcodeManager = emdkManager.getInstance(FEATURE_TYPE.BARCODE) as BarcodeManager
        if (barcodeManager != null){
            barcodeManager!!.addConnectionListener(this)
        }
        //enumerateScannerDevices()
        initScanner()
    }

    override fun onClosed() {
        if (emdkManager != null){
            emdkManager!!.release()
            emdkManager = null
        }
    }

    override fun onStatus(statusData: StatusData) {
        val state = statusData.state
        when (state){
            StatusData.ScannerStates.IDLE -> {
                statusString = statusData.friendlyName + " je zapnuty a ceka...."
                if (!scanner!!.isReadPending()) {
                    val scannerConfig = scanner!!.config
                    Log.d("scanpr","Pred ${scannerConfig.decoderParams.i2of5.enabled.toString()}")
                    setDecoders()
                    //scannerConfig.decoderParams.i2of5.enabled = true
                    //scannerConfig.decoderParams.i2of5.length1 = 14
                    //scannerConfig.decoderParams.i2of5.reportCheckDigit = true
                    //scannerConfig.decoderParams.i2of5.verifyCheckDigit = ScannerConfig.CheckDigitType.NO
                    //scannerConfig.decoderParams.code128.enabled = true
                    //scannerConfig.decoderParams.code39.enabled = true
                    //scannerConfig.decoderParams.code93.enabled = true
                    //Log.d("scanpr","Po ${scannerConfig.decoderParams.i2of5.enabled.toString()}")
                    //scanner!!.config(scannerConfig)
                    try {
                        Log.d("scanpr","Po ${scannerConfig.decoderParams.i2of5.enabled.toString()}")
                        scanner!!.read()
                    } catch (e: ScannerException) {
                        statusString = e.message.toString()
                        statusTextView!!.text = statusString
                    }
                }
            }
            StatusData.ScannerStates.WAITING -> {

            }
            StatusData.ScannerStates.SCANNING -> {

            }
            StatusData.ScannerStates.DISABLED -> {

            }
            StatusData.ScannerStates.ERROR -> {

            }
            else -> {

            }
        }
    }

    override fun onData(scanDataCollection: ScanDataCollection) {
        if (scanDataCollection != null && scanDataCollection.result == ScannerResults.SUCCESS){
            val scanData = scanDataCollection.scanData
            for (data in scanData){
                val dataString = data.data
                try {
                    AsyncDataUpdate().execute(dataString)
                } catch (e: IOException){
                    statusTextView.text = e.message
                }

            }
        }

    }

    override fun onConnectionChange(scannerInfo: ScannerInfo, connectionState: ConnectionState) {
        val status: String
        var scannerName = ""
        val statusExtScanner = connectionState.toString()
        val scannerNameExtScanner = scannerInfo.friendlyName
        if (deviceList!!.size != 0){
            scannerName = deviceList!![scannerIndex].friendlyName
        }
        if (scannerName.equals(scannerNameExtScanner, ignoreCase = true)){
            when(connectionState){
                BarcodeManager.ConnectionState.CONNECTED ->{
                    deInitScanner()
                    initScanner()
                    //setTrigger()
                    setDecoders()
                }
                BarcodeManager.ConnectionState.DISCONNECTED -> {
                    deInitScanner()
                }
            }
            status = "$scannerNameExtScanner:$statusExtScanner"
            AsyncStatusUpdate().execute(status)
        } else {
            status = "$statusString $scannerNameExtScanner:$statusExtScanner"
            AsyncStatusUpdate().execute(status)
        }


//        TODO("Not yet implemented")
    }

    override fun onDestroy() {
        super.onDestroy()
        if (emdkManager != null){
            emdkManager!!.release()
            emdkManager = null
        }
    }
    private fun deInitScanner(){
        if(scanner != null){
            try {
                scanner!!.cancelRead()
                scanner!!.disable()
            } catch (e: Exception){
                statusTextView!!.text = "Status" + e.message
            }
            try {
                scanner!!.removeDataListener(this)
                scanner!!.removeStatusListener(this)
            } catch (e: Exception){
                statusTextView!!.text = "Status" + e.message
            }
            try {
                scanner!!.release()
            } catch (e: Exception){
                statusTextView!!.text = "Status: " + e.message
            }
            scanner = null
        }
    }

    override fun onPause() {
        super.onPause()
        deInitScanner()
        // Remove connection listener
        if (barcodeManager != null) {
            barcodeManager!!.removeConnectionListener(this)
            barcodeManager = null
            deviceList = null
        }

        // Release the barcode manager resources
        if (emdkManager != null) {
            emdkManager!!.release(FEATURE_TYPE.BARCODE)
        }
    }

    override fun onResume() {
        super.onResume()
        if (emdkManager != null){
            barcodeManager = emdkManager!!.getInstance(FEATURE_TYPE.BARCODE) as BarcodeManager
            if (barcodeManager != null){
                barcodeManager!!.addConnectionListener(this)
            }
            //enumerateScannerDevices()

            initScanner()
            //setTrigger()
            setDecoders()
        }
    }
    private fun setDecoders(){
        if (scanner == null){
            initScanner()
        }
        if (scanner != null && scanner!!.isEnabled){
            try {
                val config = scanner!!.config
                config.decoderParams.i2of5.enabled = true
                Log.d("scantest", config.decoderParams.i2of5.enabled.toString())
                //config.decoderParams.ean8.enabled = true
                config.decoderParams.ean13.enabled = true
                config.decoderParams.code39.enabled = true
                config.decoderParams.code128.enabled = true
            } catch (e: ScannerException){
                statusTextView!!.text = "Status: " + e.message
            }
        }
    }

//    private fun enumerateScannerDevices(){
//        if (barcodeManager != null){
//            val friendlyNameList = ArrayList<String>()
//            var spinnerIndex = 0
//            deviceList = barcodeManager!!.supportedDevicesInfo
//            Log.d("spinner", deviceList.toString())
//            if (deviceList != null && deviceList!!.size != 0){
//                val it = deviceList!!.iterator()
//                while (it.hasNext()){
//                    val scnInfo = it.next()
//                    friendlyNameList.add(scnInfo.friendlyName)
//                    if (scnInfo.isDefaultScanner){
//                        defaultIndex = spinnerIndex
//                    }
//                    ++spinnerIndex
//                }
//            } else {
//                statusTextView!!.text = "Status: " + "Nepovedlo se nacist typ scanneru"
//            }
//        }
//    }

    private fun initScanner(){
        if (scanner == null){

//            if (deviceList != null && deviceList!!.size != 0){
//                scanner = barcodeManager!!.getDevice(deviceList!![scannerIndex])
//            } else {
//                statusTextView!!.text = "Status: Nepodarilo se nacist typ skeneru"
//                return
//            }
            scanner = barcodeManager!!.getDevice(BarcodeManager.DeviceIdentifier.DEFAULT)
            if (scanner != null){
                scanner!!.addDataListener(this)
                scanner!!.addStatusListener(this)
                try {
                    scanner!!.enable()
                } catch (e:ScannerException){
                    statusTextView!!.text = "Status: " + e.message
                    deInitScanner()
                }
            }else{
                statusTextView!!.text = "Status: Problem s inicializaci scanneru"
            }
        }
    }

//    private fun setTrigger(){
//        if (scanner == null){
//            initScanner()
//        }
//        if (scanner != null){
//            scanner!!.triggerType = Scanner.TriggerType.HARD
//        }
//    }

//    private fun addSpinnerScannerDevicesListener(){
//        if (scanner == null){
//            deInitScanner()
//            initScanner()
//            setTrigger()
//            setDecoders()
//        }
//    }



    private fun startScan(){
        if(scanner == null){
            initScanner()
        }
        if (scanner != null){
            try {
                if (scanner!!.isEnabled){
                    scanner!!.read()
                }else {
                    statusTextView!!.text = "Status: Scanner nefunguje"
                }
            } catch (e: ScannerException){
                statusTextView.text = "Status: Prilozte carovy kod"
            }
        }
    }

    private fun stopScan(){
        if (scanner != null) {
            try {
                scanner!!.cancelRead()
            } catch (e: ScannerException){
                statusTextView!!.text = "Status: " + e.message
            }
        }
    }



    private inner class AsyncStatusUpdate : AsyncTask<String, Void, String>(){
        override fun doInBackground(vararg params: String): String {
            return params[0]
        }

        override fun onPostExecute(result: String?) {
            statusTextView!!.text = "Status: $result"
        }
    }




}