package com.pors.coopreaderlast.features.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.pors.coopreaderlast.api.SybaseApi
import com.pors.coopreaderlast.data.*
import com.pors.coopreaderlast.databinding.ActivityLoginBinding
import com.pors.coopreaderlast.features.button.ButtonActivity
import com.pors.coopreaderlast.features.zebraID.ZebraActivity
import com.pors.coopreaderlast.util.Resource
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_login.*

const val EXTRA_MESSAGE = "Message"

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {
    private val viewModel : LoginViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.btLogin.setOnClickListener{
            //viewModel.updZebraSTest(15,1)

            loginUser(view)
        }
        binding.btBackZebra.setOnClickListener{
            val intent = Intent(this, ZebraActivity::class.java)
            startActivity(intent)
        }
        //binding.btSetting.setOnClickListener { openEdit(view) }
        /*val BASE_IP_ADDRESS = "http://10.0.1.23:5000/"
        val zebra = 1
        val prac = 2
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        sharedPreferences.edit().putString("ipAddress", BASE_IP_ADDRESS).apply()
        sharedPreferences.edit().putInt("idZebra", zebra).apply()
        sharedPreferences.edit().putInt("idPrac", prac).apply()*/



        binding.apply {

            viewModel.skladnici.observe(this@LoginActivity) { result ->
                val skladnici: List<Skladnik>? = result.data
                if (skladnici != null) {
                    Log.e("Skladnici", skladnici.size.toString())
                }
                else{
                    Log.e("Skladnici", "NULL")
                }
                progressBarLogin.isVisible =
                    result is Resource.Loading && result.data.isNullOrEmpty()
                textViewProgressBar.isVisible =
                    result is Resource.Loading && result.data.isNullOrEmpty()
                textViewProgressBar.text = "Nahrávám data z databáze do čtečky"
                textViewErrorLogin.isVisible =
                    result is Resource.Error && result.data.isNullOrEmpty()
                textViewErrorLogin.text = result.error?.localizedMessage
                textViewErrorLogin.isVisible = result is Resource.Success
                textViewErrorLogin.text = "Data nahrána"

            }
        }

    }

    fun loginUser(view: View){
        val userIdText = insertLogin.text.toString()
        val passwordText = insertHeslo.text.toString()
        if (userIdText.isEmpty() || passwordText.isEmpty()){
//            startActivity(Intent(this, ButtonActivity::class.java))
            Toast.makeText(this, "Vyplňte všechna pole", Toast.LENGTH_SHORT).show()
        } else {
            val login = userIdText.toString()
            val password = passwordText.toString()

            var dotaz = viewModel.resultLogin(login, password)
            if (dotaz == null){
                Toast.makeText(this, "Špatně zadané údaje", Toast.LENGTH_LONG).show()
            } else{
                val prac = dotaz.prac.toInt()
                val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
                sharedPreferences.edit().putString("ipAddress", SybaseApi.BASE_URL).apply()
                sharedPreferences.edit().putInt("idPrac", prac).apply()
                startActivity(Intent(this, ButtonActivity::class.java))
            }

            //TODO Perform Query save from table cis06zebras - Skladnik values prac and zebra to SharedPreferences
            //TODO these values I will need later for SQL STATEMENT
            //TODO open Button Activity
        }

    }




    /*fun sendMessage(view: View) {
        val intent = Intent(this,DokladActivity::class.java).apply {
            putExtra(EXTRA_MESSAGE, "Ahoj")
        }
        startActivity(intent)
    }

    fun openEdit(view: View){
        val intent = Intent(this, EditActivity::class.java)
        startActivity(intent)
    }*/
}



