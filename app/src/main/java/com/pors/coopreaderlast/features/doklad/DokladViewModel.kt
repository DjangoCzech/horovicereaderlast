package com.pors.coopreaderlast.features.doklad

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.pors.coopreaderlast.data.DokladTuple
import com.pors.coopreaderlast.data.HlavickaTuple
import com.pors.coopreaderlast.data.SybaseDao
import com.pors.coopreaderlast.data.SybaseRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import java.util.concurrent.Flow
import javax.inject.Inject

@HiltViewModel
class DokladViewModel @Inject constructor(
    private val repository: SybaseRepository
): ViewModel(){
    val dokladyPolozky = repository.getAllHlavicky().asLiveData()

    fun getAllHlavickyByZebra(zebra: Int): LiveData<List<DokladTuple>>{
        return repository.getAllHlavickyByZebra(zebra).asLiveData()
    }


}

//@HiltViewModel
//class DokladViewModel @Inject constructor(
//    repository: SybaseRepository
//):ViewModel() {
//    val getSkladyFromPolozky = repository.getAllSkladFromPolozka().asLiveData()
//
//
//}