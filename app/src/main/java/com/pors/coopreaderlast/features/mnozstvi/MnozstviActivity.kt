package com.pors.coopreaderlast.features.mnozstvi

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.activity.viewModels
import com.pors.coopreaderlast.R
import com.pors.coopreaderlast.data.DokladTuple
import com.pors.coopreaderlast.data.Polozka
import com.pors.coopreaderlast.data.PolozkaTuple

import com.pors.coopreaderlast.databinding.ActivityMnozstviBinding
import com.pors.coopreaderlast.features.polozka.PolozkaActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_mnozstvi.*
import kotlinx.android.synthetic.main.activity_mnozstvi.view.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Response
import java.io.IOException

@AndroidEntryPoint
class MnozstviActivity : AppCompatActivity() {
    private var idZebraPref: Int = 0
    private var savedIP: String? = null
    private var savedPrac: Int = 0
    private var pvp06pkIntent: String? = null
    private var idExp: String? = ""
    private var dokladIntent: String = ""
    private var resultProdString: String? = ""
    //intent.putExtra("pvp06pkIntent", polozkaDoklad.pvp06pk)
    //intent.putExtra("idExp", polozkaDoklad.exp)
    private val viewModelMnozstvi: MnozstviViewModel by viewModels()
    private var pvp06pkUpd: String = ""
    private var valueSend: Int = 0
    var idPositionItem: Int = 0
    var maxSize: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMnozstviBinding.inflate(layoutInflater)
        //setContentView(R.layout.activity_doklad)
        setContentView(binding.root)
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        dokladIntent = intent.getStringExtra("doklad").toString()
        idZebraPref = sharedPreferences.getInt("idZebra",1)
        savedIP = sharedPreferences.getString("ipAddress","http://10.0.1.23:5000/")
        savedPrac = sharedPreferences.getInt("idPrac",1)
        pvp06pkIntent = intent.getStringExtra("pvp06pkIntent")
        idExp = intent.getStringExtra("idExp").toString()
        resultProdString = intent.getStringExtra("resultProd").toString()
        idPositionItem = intent.getIntExtra("PositionItem", 0)
        maxSize = sharedPreferences.getInt("maxSize", 0)

        val readEAN = intent.getStringExtra("ReadEAN")

        val polozkaIntentTuple = intent.getSerializableExtra("PolozkaTuple") as Polozka

        //Toast.makeText(this, "Max velikost $maxSize, idExp je $idExp",Toast.LENGTH_SHORT).show()

        //polozkaIntentTuple.mnoz_obj?.let { binding.editTextNumberDecimal.setText(it.toInt()) }
        val mnozInt: Int = polozkaIntentTuple.mnoz_obj!!.toInt()
        val editMnoz: Int = polozkaIntentTuple.mnoz_obj!!.toInt() - polozkaIntentTuple.mnoz_vyd!!.toInt()
        Log.d("mnozint", mnozInt.toString())
        binding.editTextNumberDecimal.setText(editMnoz.toString())
        binding.textViewVBAL.text = "VBAL: ${polozkaIntentTuple.veb}"
        binding.textViewPOC.text = "POC: ${polozkaIntentTuple.poc2}"
        binding.textViewPopisEAN.text = readEAN.toString()
        if (readEAN.isNullOrEmpty()){
            val nazevEan = polozkaIntentTuple.reg
            binding.textViewStatus.text = viewModelMnozstvi.getEANByREG(nazevEan.toString())
        }else {
            val nazevEAN = viewModelMnozstvi.infoByEan(readEAN.toString())
            binding.textViewStatus.text = nazevEAN.naz
        }


        binding.tableMn.tv_mnoz.text = polozkaIntentTuple.mnoz_obj.toString()
        binding.tableMn.tv_mnoz_vyd.text = polozkaIntentTuple.mnoz_vyd.toString()

        binding.button.setOnClickListener{
            super.onBackPressed()
        }



        binding.btSave.setOnClickListener {
            val valueAdd = editTextNumberDecimal.text.toString().toInt()
            val valueMNV = polozkaIntentTuple.mnoz_vyd.toInt()
            valueSend = valueAdd + valueMNV
            updStavPolozka(valueSend.toInt(), savedPrac, pvp06pkIntent.toString())
            if (idPositionItem == maxSize) {
                idPositionItem = idPositionItem
            } else{
                idPositionItem = idPositionItem + 1
            }
            val intent = Intent(this, PolozkaActivity::class.java).apply {
                putExtra("PositionItemPlus",idPositionItem )
                putExtra("doklad", dokladIntent)
                putExtra("idExp", idExp)
                putExtra("resultProd", resultProdString)
            }
            startActivity(intent)
            //super.onBackPressed()
        }
        //Toast.makeText(this, "Doklad $dokladIntent a idExp $idExp", Toast.LENGTH_LONG).show()
    }

//    override fun onBackPressed() {
//        super.onBackPressed()
//        val intent = Intent()
//        intent.putExtra("testIntent", 5)
//        setResult(RESULT_OK, intent)
//        finish()
//    }

    fun updStavPolozka(mnozVyd:Int, prac: Int, pvp06pk: String){
        val url = "${savedIP}updStavPolozka?mnoz_vyd=$mnozVyd&prac=$prac&pvp06pk=$pvp06pk"
        //Toast.makeText(this, url, Toast.LENGTH_LONG).show()
        try {
            val request = okhttp3.Request.Builder().url(url).build()
            val client = OkHttpClient()
            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Handler(Looper.getMainLooper()).post {
                        Toast.makeText(
                            this@MnozstviActivity,
                            "Nejaky problem s pripojenim: ${e.message}",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    Log.d("GET", e.message.toString())

                }
                override fun onResponse(call: Call, response: Response) {
                    val body = response.code
//                    Handler(Looper.getMainLooper()).post {
//                        Toast.makeText(this@MnozstviActivity, "Response code $body", Toast.LENGTH_LONG).show()
//                    }
                    if (body == 200){
                        viewModelMnozstvi.updStavMnozstviPolozky(valueSend, savedPrac.toString(), pvp06pkIntent.toString())
                    } else {
                        Handler(Looper.getMainLooper()).post {
                            Toast.makeText(this@MnozstviActivity, "Problém s připojením do databáze", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            })
        } catch (e: IOException) {
            Handler(Looper.getMainLooper()).post {
                Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
            }
        }
    }


}