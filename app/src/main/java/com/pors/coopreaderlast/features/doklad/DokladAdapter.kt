package com.pors.coopreaderlast.features.doklad


import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.view.menu.MenuBuilder
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.pors.coopreaderlast.R
import com.pors.coopreaderlast.data.DokladTuple
import com.pors.coopreaderlast.databinding.DokladyItemBinding

class DokladAdapter(var context: Context?, private val listener: OnItemClickListener): ListAdapter<DokladTuple, DokladAdapter.PolozkaViewHolder>(DiffCallback()) {
    private var selectedItemPosition: Int = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PolozkaViewHolder {
        val binding = DokladyItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PolozkaViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PolozkaViewHolder, position: Int) {
        val currentItem = getItem(position)
        if (currentItem != null) {
            holder.bind(currentItem)
        }
        if (selectedItemPosition == position){
            holder.itemView.setBackgroundColor(Color.parseColor("#DA745A"))
        } else {
            holder.itemView.setBackgroundColor(Color.TRANSPARENT)
        }

//        if (holder.itemView.isSelected){
//            context?.let { ContextCompat.getColor(it, R.color.background_color_grey) }
//                ?.let { holder.itemView.setBackgroundColor(it) }
//        } else {
//            context?.let { ContextCompat.getColor(it, R.color.white) }
//                ?.let { holder.itemView.setBackgroundColor(it) }
//        }


//        holder.itemView.setOnClickListener {
//            selectedItemPosition = position
//            notifyDataSetChanged()
//        }
//        if (selectedItemPosition == position)
//            holder.itemView.setBackgroundColor(Color.parseColor("#DC746C"))
//        else
//            holder.itemView.setBackgroundColor(Color.parseColor("#FFFFFF"))
    }

    inner class PolozkaViewHolder(private val binding: DokladyItemBinding): RecyclerView.ViewHolder(binding.root) {
        init {
            binding.root.setOnClickListener{
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION){
                    val item = getItem(position)
                    if (item != null){
                        listener.onItemClick(item)
                    }
                }
                notifyItemChanged(selectedItemPosition)
                selectedItemPosition = absoluteAdapterPosition
                notifyItemChanged(selectedItemPosition)
            }
            binding.root.setOnLongClickListener{
                val positionLong = bindingAdapterPosition
                if (positionLong != RecyclerView.NO_POSITION){
                    val itemLong = getItem(positionLong)
                    if (itemLong != null){
                        listener.onLongClick(itemLong)
                    }
                }
                true
            }
        }
        fun bind(polozkaHlavicka: DokladTuple){
            binding.apply {
                tvDOKL.text = polozkaHlavicka.doklad.toString()
                tvORG.text = polozkaHlavicka.odj.toString()
                tvDATUM.text = polozkaHlavicka.datuct.toString()
            }

        }

    }
    interface OnItemClickListener{
        fun onItemClick(polozkaHlavicka: DokladTuple)
        fun onLongClick(polozkaHlavicka: DokladTuple)

    }

    class DiffCallback: DiffUtil.ItemCallback<DokladTuple>(){
        override fun areItemsTheSame(oldItem: DokladTuple, newItem: DokladTuple) =
            oldItem.doklad == newItem.doklad

        override fun areContentsTheSame(oldItem: DokladTuple, newItem: DokladTuple) =
            oldItem == newItem
    }
}