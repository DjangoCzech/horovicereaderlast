package com.pors.coopreaderlast.features.zebraID

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.pors.coopreaderlast.data.SybaseRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ZebraViewModel @Inject constructor(
    private val repository: SybaseRepository
):ViewModel(){
    val skladnici = repository.getSkladnik().asLiveData()

    fun getAllZebra(): LiveData<List<Int>>{
        return repository.getAllZebra()
    }
}