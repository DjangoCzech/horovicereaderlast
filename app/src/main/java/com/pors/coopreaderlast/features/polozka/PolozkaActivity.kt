@file:Suppress("DEPRECATION")

package com.pors.coopreaderlast.features.polozka

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.drawable.ColorDrawable
import android.os.*
import android.util.Log
import android.view.ContextMenu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.pors.coopreaderlast.R
import com.pors.coopreaderlast.databinding.ActivityPolozkaBinding
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.view.children
import androidx.core.view.get
import androidx.recyclerview.widget.RecyclerView
import com.pors.coopreaderlast.data.DokladTuple
import com.pors.coopreaderlast.data.Polozka
import com.pors.coopreaderlast.data.PolozkaTuple
import com.pors.coopreaderlast.data.SybaseRepository
import com.pors.coopreaderlast.features.doklad.DokladActivity
import com.pors.coopreaderlast.features.doklad.DokladAdapter
import com.pors.coopreaderlast.features.mnozstvi.MnozstviActivity

import dagger.hilt.android.AndroidEntryPoint

import com.symbol.emdk.EMDKManager
import com.symbol.emdk.EMDKResults
import com.symbol.emdk.EMDKManager.EMDKListener
import com.symbol.emdk.EMDKManager.FEATURE_TYPE
import com.symbol.emdk.barcode.BarcodeManager
import com.symbol.emdk.barcode.BarcodeManager.ConnectionState
import com.symbol.emdk.barcode.BarcodeManager.ScannerConnectionListener
import com.symbol.emdk.barcode.ScanDataCollection
import com.symbol.emdk.barcode.Scanner
import com.symbol.emdk.barcode.ScannerException
import com.symbol.emdk.barcode.ScannerInfo
import com.symbol.emdk.barcode.ScannerResults
import com.symbol.emdk.barcode.Scanner.DataListener
import com.symbol.emdk.barcode.Scanner.StatusListener
import com.symbol.emdk.barcode.StatusData
import kotlinx.android.synthetic.main.activity_polozka.*
import kotlinx.android.synthetic.main.activity_polozka.bt_Save
import kotlinx.android.synthetic.main.activity_polozka.textViewPopis
import kotlinx.android.synthetic.main.activity_polozka.textViewProdejna
import kotlinx.android.synthetic.main.activity_polozka.textViewStatus
import kotlinx.android.synthetic.main.simple_item_spinner.*
//import kotlinx.android.synthetic.main.activity_polozka_test.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Response
import java.io.IOException
import java.lang.Exception

@AndroidEntryPoint
class PolozkaActivity: AppCompatActivity(), PolozkaAdapter.OnItemClickListener, EMDKListener, StatusListener, DataListener, ScannerConnectionListener {
    private val polozkaViewModel: PolozkaViewModel by viewModels()
    private var emdkManager: EMDKManager? = null
    private var barcodeManager: BarcodeManager? = null
    private var scanner: Scanner? = null
    private val recyclerViewObjednavka by lazy { findViewById<RecyclerView>(R.id.recycler_view_objednavka) }
    private val eanTextView by lazy { findViewById<TextView>(R.id.textViewEAN)}
    private val statusTextView by lazy { findViewById<TextView>(R.id.textViewStatus) }
    private var deviceList: List<ScannerInfo>? = null
    private var scannerIndex = 0 // Keep the selected scanner
    private var defaultIndex = 0 // Keep the default scanner
    private var triggerIndex = 0
    private var dataLength = 0
    private var statusString = ""
    private lateinit var sharedPreferences: SharedPreferences
    private var idZebraPref: Int = 1
    private var dokladIntent: String = ""
    private lateinit var savedIP: String
    var savedPrac: Int = 0
    var resultEanReg: String = ""
    var regRes: String = ""
    var positionItem: Int = 0
    var idPositionItem: Int = 0
    var maxSize: Int = 0
    var resultProdString: String = ""
    var testIntent: Int = 0
    var index: Int = -1
    var top: Int = -1
    lateinit var binding:ActivityPolozkaBinding
    private var mLayoutManager: LinearLayoutManager? = null
    private var idExp: String = ""


    private val triggerStrings = arrayOf("HARD", "SOFT")
    class Items(val items: List<String>)

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPolozkaBinding.inflate(layoutInflater)
        dokladIntent = intent.getStringExtra("doklad").toString()
        deviceList = ArrayList()
        sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        idZebraPref = sharedPreferences.getInt("idZebra",1)
        idExp = intent.getStringExtra("idExp").toString()
        idPositionItem = intent.getIntExtra("PositionItemPlus",0)
        if (idPositionItem == -1) idPositionItem = 0
        savedIP = sharedPreferences.getString("ipAddress","http://10.0.1.23:5000/").toString()
        savedPrac = sharedPreferences.getInt("idPrac",1)

        setContentView(binding.root)
        //setContentView(R.layout.activity_polozka)
        //Toast.makeText(this, "Id pos je ${idPositionItem}", Toast.LENGTH_LONG).show()
        val polozkaAdapter = PolozkaAdapter(idPositionItem, this)
        polozkaAdapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY

        maxSize = sharedPreferences.getInt("maxSize", 0)

        resultProdString = intent.getStringExtra("resultProd").toString()

        binding.button.setOnClickListener{
            val intent = Intent(this, DokladActivity::class.java)
            intent.putExtra("doklad", dokladIntent)
            intent.putExtra("idExp", idExp)
            startActivity(intent)
            //super.onBackPressed()
        }
        //Toast.makeText(this, "Exp je ${idExp}", Toast.LENGTH_LONG).show()


        textViewProdejna.text = resultProdString


        registerForContextMenu(tv_Table_Kde)

        try {
            val results = EMDKManager.getEMDKManager(applicationContext, this)
            if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
                textViewStatus!!.text = "Status: Problem s EMDKmanazerem"
                return
            }
        } catch (e:Exception){
            textViewStatus!!.text = e.message
        }


        mLayoutManager = LinearLayoutManager(this@PolozkaActivity)
//        recyclerViewObjednavka.adapter = polozkaAdapter
//        recyclerViewObjednavka.layoutManager = mLayoutManager
//        polozkaViewModel.getall(dokladIntent.toString(),idExp.toString()).observe(this){
//            polozkaAdapter.submitList(it)
//        }

        binding.apply {
            recyclerViewObjednavka.apply {
                adapter = polozkaAdapter
                layoutManager = mLayoutManager
            //LinearLayoutManager(this@PolozkaActivity)

            }
            polozkaViewModel.getall(dokladIntent.toString(),idExp.toString() ).observe(this@PolozkaActivity){
//                val updatedList = it.mapIndexed{ index, polozka ->
//                    if (idPositionItem == idPositionItem){
//                        polozka.copy(selected = true)
//                    } else {
//                        polozka
//                    }
//                }
                polozkaAdapter.submitList(it)
                //polozkaAdapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY

            }
        }

//        val selectedItem = polozkaAdapter.getCurrentItem()
//        Toast.makeText(this, "Reg je ${selectedItem}", Toast.LENGTH_LONG).show()
        val actionBar = supportActionBar
        actionBar!!.title = "$dokladIntent"


        //recycler_view_objednavka.findViewHolderForAdapterPosition(0)?.itemView?.performClick()
//        val testint = polozkaAdapter.getItemId(idPositionItem)
//        Toast.makeText(this, "Id polozky je ${testint}", Toast.LENGTH_LONG).show()

        bt_Save.setOnClickListener{
            val countStav = polozkaViewModel.countStavZebrap(dokladIntent.toString(), idExp.toString())
            if (countStav > 0){
                AlertDialog.Builder(this)
                    .setTitle("Uzavíráte doklad")
                    .setMessage("Ještě zbývají položky k expedici. Přejete si opravdu uzavřít doklad?")
                    .setPositiveButton(
                        android.R.string.yes,
                        DialogInterface.OnClickListener { dialog, which ->
                            if (dokladIntent != null) {
                                updUkonceniDokladu(dokladIntent.toInt(),idExp!!.toInt() )
                                updUkonceniDokladuP(dokladIntent.toInt(), idExp!!.toInt())
                                startActivity(Intent(this, DokladActivity::class.java))
                            } else {
                                Toast.makeText(this, "Nepodařilo se načíst číslo dokladu", Toast.LENGTH_SHORT).show()
                            }
                            Toast.makeText(this, "Doklad č. $dokladIntent uzavřen", Toast.LENGTH_LONG).show()
                            // Continue with delete operation
                        }) // A null listener allows the button to dismiss the dialog and take no further action.
                    .setNegativeButton(android.R.string.no,
                        DialogInterface.OnClickListener{ dialog, which ->
                            Toast.makeText(this, "Pokračujeme", Toast.LENGTH_SHORT).show()
                        })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show()
            } else {
                if (dokladIntent != null) {
                    updUkonceniDokladu(dokladIntent.toInt(),idExp!!.toInt() )
                    updUkonceniDokladuP(dokladIntent.toInt(), idExp!!.toInt())
                    Toast.makeText(this, "Doklad č. $dokladIntent uzavřen", Toast.LENGTH_LONG).show()
                    startActivity(Intent(this, DokladActivity::class.java))
                } else {
                    Toast.makeText(this, "Nepodařilo se načíst číslo dokladu", Toast.LENGTH_SHORT).show()
                }
            }
        }
//        Toast.makeText(this,"Zobrazi se tato zprava?", Toast.LENGTH_SHORT).show()
//        if (testIntent == 0){
//            Toast.makeText(this,"Neco se pokazilo hodnota je $testIntent", Toast.LENGTH_SHORT).show()
//        } else {
//            Toast.makeText(this,"Hodnota je $testIntent", Toast.LENGTH_SHORT).show()
//        }
    }






//    override fun onStart() {
//        super.onStart()
//        idPositionItem = intent.getIntExtra("PositionItemPlus",0)
//        if (idPositionItem == -1) idPositionItem = 0
//        val polozkaAdapter = PolozkaAdapter(idPositionItem, this)
//        val selectedItem = polozkaAdapter.getCurrentItem()
//        Toast.makeText(this, "Reg vybrane polozky je ${selectedItem.reg}", Toast.LENGTH_LONG).show()
//    }

    //    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (resultCode != Activity.RESULT_OK) return
//        when(requestCode){
//            1 -> if (data != null) {
//                testIntent = data.getIntExtra("testIntent",0)
//            } else -> testIntent = 0
//        }
//    }

    fun getAllPolozkyByDoklad(){

        //val binding = ActivityPolozkaBinding.inflate(layoutInflater)
        val binding = ActivityPolozkaBinding.inflate(layoutInflater)
        setContentView(binding.root)
        registerForContextMenu(tv_Table_Kde)
        val polozkaAdapter = PolozkaAdapter(idPositionItem,this)
        val idExp = intent.getStringExtra("idExp")
        binding.apply {
            recyclerViewObjednavka.apply {
                adapter = polozkaAdapter
                layoutManager = LinearLayoutManager(this@PolozkaActivity)
            }
            polozkaViewModel.getall(dokladIntent, idExp.toString()).observe(this@PolozkaActivity){
                polozkaAdapter.submitList(it)
            }
        }

    }

    fun getAllPolozkyByDokladNesouhlasiMnozstvi(){

        //val binding = ActivityPolozkaBinding.inflate(layoutInflater)
        val binding = ActivityPolozkaBinding.inflate(layoutInflater)
        setContentView(binding.root)
        registerForContextMenu(tv_Table_Kde)
        val polozkaAdapter = PolozkaAdapter(idPositionItem, this)
        val idExp = intent.getStringExtra("idExp")
        binding.apply {
            recyclerViewObjednavka.apply {
                adapter = polozkaAdapter
                layoutManager = LinearLayoutManager(this@PolozkaActivity)
            }
            polozkaViewModel.getAllPolozkyByDokladNesouhlasiMnozstvi(dokladIntent, idExp.toString()).observe(this@PolozkaActivity){
                polozkaAdapter.submitList(it)
            }
        }

    }

    fun getAllPolozkyByDokladSplnene(){

        //val binding = ActivityPolozkaBinding.inflate(layoutInflater)
        val binding = ActivityPolozkaBinding.inflate(layoutInflater)
        setContentView(binding.root)
        registerForContextMenu(tv_Table_Kde)
        val polozkaAdapter = PolozkaAdapter(idPositionItem, this)
        val idExp = intent.getStringExtra("idExp")
        binding.apply {
            recyclerViewObjednavka.apply {
                adapter = polozkaAdapter
                layoutManager = LinearLayoutManager(this@PolozkaActivity)
            }
            polozkaViewModel.getAllPolozkyByDokladSplnene(dokladIntent, idExp.toString()).observe(this@PolozkaActivity){
                polozkaAdapter.submitList(it)
            }
        }

    }

    fun getAllPolozkyByDokladNezpracovane(){
        val binding = ActivityPolozkaBinding.inflate(layoutInflater)
        setContentView(binding.root)
        registerForContextMenu(tv_Table_Kde)
        val polozkaAdapter = PolozkaAdapter( idPositionItem, this)
        val idExp = intent.getStringExtra("idExp")
        binding.apply {
            recyclerViewObjednavka.apply {
                adapter = polozkaAdapter
                layoutManager = LinearLayoutManager(this@PolozkaActivity)
            }
            polozkaViewModel.getAllPolozkyByDokladNezpracovane(dokladIntent, idExp.toString()).observe(this@PolozkaActivity){
                polozkaAdapter.submitList(it)
            }
        }

    }

    fun updUkonceniDokladu(doklad:Int, exp: Int){
        val url = "${savedIP}updUkonceniDokladu?doklad=$doklad&exp=$exp"
        //Toast.makeText(this, url, Toast.LENGTH_SHORT).show()
        try {
            val request = okhttp3.Request.Builder().url(url).build()
            val client = OkHttpClient()
            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Handler(Looper.getMainLooper()).post {
                        Toast.makeText(
                            this@PolozkaActivity,
                            "Nejaky problem s pripojenim: ${e.message}",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    Log.d("GET", e.message.toString())

                }
                override fun onResponse(call: Call, response: Response) {
                    val body = response.code
                    if (body == 200){
                        polozkaViewModel.updStavDokladUkonceniZ(dokladIntent)
                    } else{
                        Handler(Looper.getMainLooper()!!).post {
                            Toast.makeText(
                                this@PolozkaActivity,
                                "Nepodařilo se připojit k databázi",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                }
            })
        } catch (e: IOException) {
            Handler(Looper.getMainLooper()).post {
                Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun updUkonceniDokladuP(doklad:Int, exp: Int){
        val url = "${savedIP}updUkonceniDokladuP?doklad=$doklad&exp=$exp"
        //Toast.makeText(this, url, Toast.LENGTH_SHORT).show()
        try {
            val request = okhttp3.Request.Builder().url(url).build()
            val client = OkHttpClient()
            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Handler(Looper.getMainLooper()).post {
                        Toast.makeText(
                            this@PolozkaActivity,
                            "Nejaky problem s pripojenim: ${e.message}",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    Log.d("GET", e.message.toString())

                }
                override fun onResponse(call: Call, response: Response) {
                    val body = response.code
                    if (body == 200){
                        polozkaViewModel.updStavDokladUkonceniP(dokladIntent)
                    } else {
                        Handler(Looper.getMainLooper()!!).post {
                            Toast.makeText(
                                this@PolozkaActivity,
                                "Nepodařilo se připojit k databázi",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
            })
        } catch (e: IOException) {
            Handler(Looper.getMainLooper()).post {
                Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onOpened(emdkManager: EMDKManager) {
        this.emdkManager = emdkManager
        barcodeManager = emdkManager.getInstance(FEATURE_TYPE.BARCODE) as BarcodeManager
        if (barcodeManager != null){
            barcodeManager!!.addConnectionListener(this)
        }
        initScanner()
    }


    override fun onClosed() {
        if (emdkManager != null){
            emdkManager!!.release()
            emdkManager = null
        }
    }

    override fun onStatus(statusData: StatusData) {
        val state = statusData.state
        when (state){
            StatusData.ScannerStates.IDLE -> {
                statusString = statusData.friendlyName + " je zapnuty a ceka...."
                if (!scanner!!.isReadPending()) {
                    val scannerConfig = scanner!!.config
                    scannerConfig.decoderParams.code128.enabled = true
                    scannerConfig.decoderParams.i2of5.enabled = true
                    //scannerConfig.decoderParams.d2of5.enabled = true
                    scannerConfig.decoderParams.code39.enabled = true
                    scannerConfig.decoderParams.code93.enabled = true
                    try {
                        scanner!!.read()
                    } catch (e: ScannerException) {
                        statusString = e.message.toString()
                        textViewStatus!!.text = statusString
                    }
                }
            }
            StatusData.ScannerStates.WAITING -> {

            }
            StatusData.ScannerStates.SCANNING -> {

            }
            StatusData.ScannerStates.DISABLED -> {

            }
            StatusData.ScannerStates.ERROR -> {

            }
            else -> {

            }
        }
    }
    override fun onData(scanDataCollection: ScanDataCollection) {
        if (scanDataCollection != null && scanDataCollection.result == ScannerResults.SUCCESS){
            val scanData = scanDataCollection.scanData
            for (data in scanData){
                val dataString = data.data
                AsyncDataUpdate().execute(dataString)
            }
        }

    }

    override fun onConnectionChange(scannerInfo: ScannerInfo, connectionState: ConnectionState) {
        val status: String
        var scannerName = ""
        val statusExtScanner = connectionState.toString()
        val scannerNameExtScanner = scannerInfo.friendlyName
        if (deviceList!!.size != 0){
            scannerName = deviceList!![scannerIndex].friendlyName
        }
        if (scannerName.equals(scannerNameExtScanner, ignoreCase = true)){
            when(connectionState){
                BarcodeManager.ConnectionState.CONNECTED ->{
                    deInitScanner()
                    initScanner()
                }
                BarcodeManager.ConnectionState.DISCONNECTED -> {
                    deInitScanner()
                }
            }
            status = "$scannerNameExtScanner:$statusExtScanner"
            AsyncStatusUpdate().execute(status)
        } else {
            status = "$statusString $scannerNameExtScanner:$statusExtScanner"
            AsyncStatusUpdate().execute(status)
        }


        TODO("Not yet implemented")
    }
    override fun onDestroy() {
        super.onDestroy()
        if (emdkManager != null){
            emdkManager!!.release()
            emdkManager = null
        }
    }
    private fun deInitScanner(){
        if(scanner != null){
            try {
                scanner!!.release()
            } catch (e: Exception){
                textViewStatus!!.text = "Status: " + e.message
            }
            scanner = null
        }
    }
    override fun onPause() {
        super.onPause()

//        binding.recyclerViewObjednavka.layoutManager?.onSaveInstanceState()?.let {
//            polozkaViewModel.saveRecyclerViewState(it) }
        index = mLayoutManager?.findFirstVisibleItemPosition()!!
//        Log.d("index", index.toString())
        //val v:View = binding.recyclerViewObjednavka.getChildAt(0)
        val v:View = recyclerViewObjednavka.getChildAt(0)
        Log.d("index", index.toString())
        //top = if (v==null) 0 else (v.top - binding.recyclerViewObjednavka.paddingTop)
        if (v==null){
            top = 0
        }else {
            top = v.top - recyclerViewObjednavka.paddingTop
        }
//        Log.d("index", "top: ${top.toString()}")
        //Log.d("index", "doklad: ${dokladIntent.toString()}")
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        sharedPreferences.edit().putInt("index", index).apply()
        sharedPreferences.edit().putInt("top", top).apply()
        deInitScanner()
        // Remove connection listener
        if (barcodeManager != null) {
            barcodeManager!!.removeConnectionListener(this)
            barcodeManager = null
            deviceList = null
        }

        // Release the barcode manager resources
        if (emdkManager != null) {
            emdkManager!!.release(FEATURE_TYPE.BARCODE)
        }


    }

    override fun onResume() {
        super.onResume()
//        if (polozkaViewModel.stateInitialized()){
//            binding.recyclerViewObjednavka.layoutManager?.onRestoreInstanceState(
//                polozkaViewModel.restoreRecyclerViewState()
//            )
//        }
        val handler = Handler()
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        index = sharedPreferences.getInt("index", -1)
        top = sharedPreferences.getInt("top", -1)
        Log.d("index", "top: ${top.toString()} a index: ${index.toString()}")
        //Log.d("index", "dokladResume: ${dokladIntent.toString()}")
        if (index != -1){
            //binding.recyclerViewObjednavka.smoothScrollToPosition(index)
            handler.postDelayed({
                mLayoutManager?.scrollToPositionWithOffset(index + 1 ,top)
            },200)

            //recyclerViewObjednavka.smoothScrollToPosition(index)
        }


        if (emdkManager != null){
            barcodeManager = emdkManager!!.getInstance(FEATURE_TYPE.BARCODE) as BarcodeManager
            if (barcodeManager != null){
                barcodeManager!!.addConnectionListener(this)
            }
            initScanner()
        }


    }
    private fun initScanner(){
        if (scanner == null){
            scanner = barcodeManager!!.getDevice(BarcodeManager.DeviceIdentifier.DEFAULT)
            if (scanner != null){
                scanner!!.addDataListener(this)
                scanner!!.addStatusListener(this)
                try {
                    scanner!!.enable()
                } catch (e:ScannerException){
                    textViewStatus!!.text = "Status: " + e.message
                    deInitScanner()
                }
            }else{
                textViewStatus!!.text = "Status: Problem s inicializaci scanneru"
            }
        }
    }

    private fun startScan(){
        if(scanner == null){
            initScanner()
        }
        if (scanner != null){
            try {
                if (scanner!!.isEnabled){
                    scanner!!.read()
                }else {
                    textViewStatus!!.text = "Status: Scanner nefunguje"
                }
            } catch (e: ScannerException){
                textViewStatus.text = "Status: Prilozte carovy kod"
            }
        }
    }

    private fun stopScan(){
        if (scanner != null) {
            try {
                scanner!!.cancelRead()
            } catch (e: ScannerException){
                textViewStatus!!.text = "Status: " + e.message
            }
        }
    }

    private inner class AsyncDataUpdate : AsyncTask<String, Void, String>(){
        override fun doInBackground(vararg params: String): String {
            return params[0]
        }

        override fun onPostExecute(result: String?) {
            if (result != null) {
                try {
                    textViewPopis.text = result.toString()
                    //textViewPopis.text = result.toString()
                    val intent = Intent(this@PolozkaActivity, MnozstviActivity::class.java)
                    //val countRegByEan = polozkaViewModel.getCountRegDoklad(resultEanReg,)
                    val countRegEan = polozkaViewModel.getCountRegByEAN(result)
                    if (countRegEan == 0) {
                        AlertDialog.Builder(this@PolozkaActivity)
                            .setTitle("EAN!")
                            .setMessage("Tento EAN není vůbec v nabídce.")
                            .setPositiveButton(
                                android.R.string.ok,
                                DialogInterface.OnClickListener { dialog, which ->
                                }) // A null listener allows the button to dismiss the dialog and take no further action.

                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show()
                    } else {
                    val regRes = polozkaViewModel.getRegByEAN(result)
                    val countRegDoklad = polozkaViewModel.getCountRegDoklad(regRes, dokladIntent)
                    if (countRegDoklad == 0) {
                        AlertDialog.Builder(this@PolozkaActivity)
                            .setTitle("EAN!")
                            .setMessage("Tento EAN není v příslušném dokladu č.: ${dokladIntent}")
                            .setPositiveButton(
                                android.R.string.ok,
                                DialogInterface.OnClickListener { dialog, which ->
                                }) // A null listener allows the button to dismiss the dialog and take no further action.

                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show()
                    } else {
                        val regRes = polozkaViewModel.getRegByEAN(result)
                        val getCountNaskladnene = polozkaViewModel.getCountNaskladnenePolozky(dokladIntent, idExp, 2, regRes)
                        if (getCountNaskladnene == 1) {
                            AlertDialog.Builder(this@PolozkaActivity)
                                .setTitle("Pozor")
                                .setMessage("Toto zboží již bylo v objednávce vychystáno!")
                                .setPositiveButton(
                                    android.R.string.ok,
                                    DialogInterface.OnClickListener { dialog, which ->
                                    }) // A null listener allows the button to dismiss the dialog and take no further action.
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show()
                        } else {
                            resultEanReg = polozkaViewModel.getRegByEAN(result)
                            val polozkaByReg =
                                polozkaViewModel.loadAllPolozkaByReg(resultEanReg, dokladIntent)
                            val pvp06pkIntent = polozkaByReg.pvp06pk
                            intent.putExtra("pvp06pkIntent", pvp06pkIntent)
                            intent.putExtra("doklad", dokladIntent)
                            intent.putExtra("idExp", idExp)
                            intent.putExtra("ReadEAN", result)
                            intent.putExtra("PolozkaTuple", polozkaByReg)
                            intent.putExtra("PositionItem", idPositionItem)
                            intent.putExtra("resultProd", resultProdString)

                            startActivity(intent)
                        }
                    }
                    }
                } catch (e: IOException) {
                    textViewStatus.text = "Problem s nactenim EAN kodu"
                }

            }
        }
    }

    private inner class AsyncStatusUpdate : AsyncTask<String, Void, String>(){
        override fun doInBackground(vararg params: String): String {
            return params[0]
        }

        override fun onPostExecute(result: String?) {
            textViewStatus!!.text = "Status: $result"
        }
    }

//    fun getResultReg(){
//        sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
//        val regGet = sharedPreferences.getString("reg","")
//        val resultReg = regGet?.let { polozkaViewModel.getInfoByREG(it) }
//        if (resultReg.isNullOrEmpty()){
//            textViewStatus.text = "Problem s nactenim dat."
//        } else {
//            textViewStatus.text = "$resultReg"
//        }
//
//    }

    override fun onItemClick(polozkaDoklad: Polozka, position: Int) {
        val resultReg = polozkaViewModel.getInfoByREG(polozkaDoklad.reg.toString())
        textViewStatus.text = "$resultReg"
        idPositionItem = position
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        sharedPreferences.edit().putString("reg", polozkaDoklad.reg.toString())
        val actionBar = supportActionBar
        actionBar!!.title = "${polozkaDoklad.doklad} - (${polozkaDoklad.sklad})"

        //Toast.makeText(this, "IDPolozky je $position", Toast.LENGTH_LONG).show()
    }


    override fun onLongClick(polozkaDoklad: Polozka, position: Int) {

        val getCountNaskladnene = polozkaViewModel.getCountNaskladnenePolozky(polozkaDoklad.doklad.toString(), polozkaDoklad.exp.toString(), 2, polozkaDoklad.reg.toString())
        if (getCountNaskladnene == 1) {
            AlertDialog.Builder(this@PolozkaActivity)
                .setTitle("Pozor")
                .setMessage("Toto zboží již bylo v objednávce vychystáno!")
                .setPositiveButton(
                    android.R.string.ok,
                    DialogInterface.OnClickListener { dialog, which ->
                    }) // A null listener allows the button to dismiss the dialog and take no further action.
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show()
        } else {
            idPositionItem = position
            val intent = Intent(this, MnozstviActivity::class.java)
            intent.putExtra("doklad", dokladIntent)
            intent.putExtra("pvp06pkIntent", polozkaDoklad.pvp06pk)
            intent.putExtra("idExp", idExp)
            intent.putExtra("PolozkaTuple", polozkaDoklad)
            intent.putExtra("PositionItem", idPositionItem)
            intent.putExtra("resultProd", resultProdString)

            startActivity(intent)
        }
    }

    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.polozky_menu, menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        //val infoMenu = item.menuInfo as AdapterView.AdapterContextMenuInfo
        return when (item.itemId) {
            R.id.m_allPolozky -> {
                getAllPolozkyByDoklad()
                //Toast.makeText(this, "Kliknuli jsme na vsechny",Toast.LENGTH_SHORT).show()
                true
            }
            R.id.m_splnenePolozky -> {
                getAllPolozkyByDokladSplnene()
                true
            }
            R.id.m_nezpracovanePolozky-> {
                getAllPolozkyByDokladNezpracovane()
                //Toast.makeText(this, "Kliknuli jsme na nezpracovane polozky",Toast.LENGTH_SHORT).show()
                true
            }
            R.id.m_nesMnozPolozky-> {
                getAllPolozkyByDokladNesouhlasiMnozstvi()
                //Toast.makeText(this, "Kliknuli jsme na nesouhlasi mnozstvi",Toast.LENGTH_SHORT).show()
                true
            }

            else -> super.onContextItemSelected(item)
        }
        //return super.onContextItemSelected(item)
    }
}