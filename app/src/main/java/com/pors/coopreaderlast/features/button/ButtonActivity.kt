package com.pors.coopreaderlast.features.button

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.view.isVisible
import com.pors.coopreaderlast.data.*
import com.pors.coopreaderlast.databinding.ActivityButtonBinding
import com.pors.coopreaderlast.features.doklad.DokladActivity
import com.pors.coopreaderlast.features.information.InfoActivity
import com.pors.coopreaderlast.features.informationDW.InfoDWActivity
import com.pors.coopreaderlast.features.login.LoginActivity
import com.pors.coopreaderlast.features.zebraID.ZebraActivity
import com.pors.coopreaderlast.util.Resource
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_button.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Response
import java.io.IOException

@AndroidEntryPoint
class ButtonActivity : AppCompatActivity() {
    private var savedIP: String? = null
    private var idZebraPref: Int = 1
    private var idPracPref: Int = 1
    private val viewModel: ButtonViewModel by viewModels()
    val sybaseDao: SybaseDao? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityButtonBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        idZebraPref = sharedPreferences.getInt("idZebra",1)
        idPracPref = sharedPreferences.getInt("idPrac",1)

        savedIP = sharedPreferences.getString("ipAddress","http://192.168.1.25:5000/")

        button3.setOnClickListener{
            startActivity(Intent(this, DokladActivity::class.java))
        }
        binding.btBackZebra.setOnClickListener{
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }


        btDatabase.setOnClickListener {
            binding.apply {
            viewModel.zbozi.observe(this@ButtonActivity) { result ->
                val zbozi: List<Zbozi>? = result.data
                progressBarLogin.isVisible =
                    result is Resource.Loading && result.data.isNullOrEmpty()
                textViewProgressBar.isVisible =
                    result is Resource.Loading && result.data.isNullOrEmpty()
                textViewProgressBar.text = "Nahrávám data z databáze do čtečky"
                textViewErrorLogin.isVisible =
                    result is Resource.Error && result.data.isNullOrEmpty()
                textViewErrorLogin.text = result.error?.localizedMessage
                textViewErrorLogin.isVisible = result is Resource.Success
                textViewErrorLogin.text = "Data nahrana"
            }
        }
                    }

//

        btInformation.setOnClickListener (View.OnClickListener {
            startActivity(Intent(this@ButtonActivity, InfoDWActivity::class.java))
        })

        binding.apply {
            viewModel.hlavicky.observe(this@ButtonActivity) { result ->
                val hlavicky: List<Hlavicka>? = result.data
            }
            viewModel.prodejny.observe(this@ButtonActivity) {result ->
                val prodejny: List<Prodejna>? = result.data
            }

            viewModel.polozky.observe(this@ButtonActivity) { result ->
                val polozky: List<Polozka>? = result.data
                progressBarLogin.isVisible = result is Resource.Loading && result.data.isNullOrEmpty()
                textViewProgressBar.isVisible = result is Resource.Loading && result.data.isNullOrEmpty()
                textViewProgressBar.text = "Nahrávám data z databáze do čtečky"
                textViewErrorLogin.isVisible = result is Resource.Error && result.data.isNullOrEmpty()
                textViewErrorLogin.text = result.error?.localizedMessage
                textViewErrorLogin.isVisible = result is Resource.Success
                textViewErrorLogin.text = "Data nahrana"
            }
        }
        updStavDatabaze(idPracPref, idZebraPref)


    }

    fun updStavDatabaze(idPrac: Int, idZebra:Int){
        val url = "${savedIP}updStavDatabaze?prac=${idPrac}&zebra=${idZebra}"
        //Toast.makeText(this, url, Toast.LENGTH_SHORT).show()
        try {
            val request = okhttp3.Request.Builder().url(url).build()
            val client = OkHttpClient()
            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Handler(Looper.getMainLooper()).post {
                        Toast.makeText(
                            this@ButtonActivity,
                            "Zebra: $idZebra, Nejaky problem s pripojenim: ${e.message}",
                            Toast.LENGTH_SHORT
                        ).show()
                        //startActivity(Intent(this@ButtonActivity, DokladActivity::class.java))
                    }
                    Log.d("GET", e.message.toString())

                }
                override fun onResponse(call: Call, response: Response) {
                    val body = response.body?.string()
                    val bod = response.code
                    if (bod == 200){
                        viewModel.updStavyZebraPStavPolozka()
                        viewModel.updStavyZebraZStavPolozka(idPracPref, idZebraPref)

                    } else{
                        Handler(Looper.getMainLooper()!!).post {
                            Toast.makeText(
                                this@ButtonActivity,
                                "Nepodařilo se připojit k databázi",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        }
                    Handler(Looper.getMainLooper()).post {

                        //startActivity(Intent(this@ButtonActivity, DokladActivity::class.java))
                    }
                }
            })
        } catch (e: IOException) {
            Handler(Looper.getMainLooper()).post {
                Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
            }
        }
    }
}