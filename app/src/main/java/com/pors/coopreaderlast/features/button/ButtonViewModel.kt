package com.pors.coopreaderlast.features.button

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.pors.coopreaderlast.data.SybaseRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ButtonViewModel @Inject constructor(
    repository: SybaseRepository
): ViewModel() {
    var repo = repository
    //val skladnici = repository.getSkladnik().asLiveData()
    val hlavicky = repository.getHlavicky().asLiveData()
    val polozky = repository.getPolozky().asLiveData()
    val zbozi = repository.getZbozi().asLiveData()
    val prodejny = repository.getProdejna().asLiveData()

    fun updStavyZebraPStavPolozka(){
        return repo.updStavyZebraPStavPolozka()
    }
    fun updStavyZebraZStavPolozka(prac: Int, zebra: Int){
        return repo.updStavyZebraZStavPolozka(prac, zebra)
    }


}