package com.pors.coopreaderlast.features.edit

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.pors.coopreaderlast.R
import com.pors.coopreaderlast.features.login.LoginActivity
import kotlinx.android.synthetic.main.activity_edit.*


class EditActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val savedIP = sharedPreferences.getString("ipAddress","http://10.0.1.23:5000/")

        textViewStatus.text = savedIP
        editIpAddress.setText(savedIP)

        buttonSave.setOnClickListener {saveData() }
    }

    fun openLoginActivity(){
        val intent = Intent(this, LoginActivity::class.java )
        startActivity(intent)
    }

    private fun saveData(){

        val insertedText: String = editIpAddress.text.toString()
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.apply(){
            putString("ipAddress", insertedText)
        }.apply()
        Toast.makeText(this, "IP adresa ulozena", Toast.LENGTH_LONG).show()
        //val intent = Intent(this, LoginActivity::class.java )
        //startActivity(intent)
    }

    private fun loadData(){

        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val savedString = sharedPreferences.getString("ipAddress", "http://10.0.1.23:5000/")
        textViewIpShow.text = savedString
    }
}