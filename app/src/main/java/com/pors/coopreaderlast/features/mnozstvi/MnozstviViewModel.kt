package com.pors.coopreaderlast.features.mnozstvi

import androidx.lifecycle.ViewModel
import com.pors.coopreaderlast.data.SybaseRepository
import com.pors.coopreaderlast.data.Zbozi
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MnozstviViewModel @Inject constructor(
    repository: SybaseRepository
): ViewModel() {
    var repo = repository
    fun updStavMnozstviPolozky(mnoz_vyd: Int, prac: String, pvp06pk: String){
        return repo.updStavMnozstviPolozky(mnoz_vyd, prac, pvp06pk)
    }

    fun infoByEan(ean: String): Zbozi {
        return repo.getInfoByEAN(ean)
    }
    fun getEANByREG(reg: String): String{
        return repo.getEANByREG(reg)
    }
}