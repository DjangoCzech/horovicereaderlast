package com.pors.coopreaderlast.features.doklad


import android.view.LayoutInflater
import android.view.ViewGroup
import android.graphics.Color
import android.widget.AdapterView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide.init
import com.pors.coopreaderlast.data.SkladTuple
import com.pors.coopreaderlast.databinding.SkladyItemBinding
//private val listener: AdapterView.OnItemClickListener):RecyclerView.Adapter<SkladAdapter.PolozkaViewHolder>()

class SkladAdapter(private val listener: OnItemClickListener): ListAdapter<SkladTuple, SkladAdapter.PolozkaViewHolder>(DiffCallback())
{
    var selected_position: Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PolozkaViewHolder {
        val binding = SkladyItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PolozkaViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PolozkaViewHolder, position: Int) {
        val currentItem = getItem(position)
        if (currentItem != null){
            holder.bind(currentItem)
        }


    }
//SkladyItemBinding je automaticky generovano pro sklady_item.xml
    inner class PolozkaViewHolder(private val binding: SkladyItemBinding): RecyclerView.ViewHolder(binding.root){
    init {
        binding.root.setOnClickListener{
            val position = bindingAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val item = getItem(position)
                if (item != null){
                    listener.onItemClick(item)
                }
            }

        }
    }

        fun bind(polozkaSklad: SkladTuple){
            binding.apply {
                tvSklad.text = polozkaSklad.sklad.toString()
                tvRegal.text = polozkaSklad.reg.toString()
            }
//            binding.root.setOnClickListener{
//                onSelect(polozkaSklad)
//            }
        }

    }
    interface OnItemClickListener{
        fun onItemClick(polozkaSklad: SkladTuple)
    }
    class DiffCallback: DiffUtil.ItemCallback<SkladTuple>(){
        override fun areItemsTheSame(oldItem: SkladTuple, newItem: SkladTuple) =
            oldItem.sklad == newItem.sklad

        override fun areContentsTheSame(oldItem: SkladTuple, newItem: SkladTuple) =
            oldItem == newItem
    }
}
