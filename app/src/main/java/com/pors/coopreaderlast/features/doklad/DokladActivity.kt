package com.pors.coopreaderlast.features.doklad

import android.R
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.pors.coopreaderlast.api.SybaseApi
import com.pors.coopreaderlast.api.SybaseApi.Companion.BASE_URL
import com.pors.coopreaderlast.data.DokladTuple
import com.pors.coopreaderlast.data.SkladTuple
import com.pors.coopreaderlast.databinding.ActivityDokladBinding
import com.pors.coopreaderlast.features.button.ButtonActivity
import com.pors.coopreaderlast.features.polozka.PolozkaActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_doklad.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Response
import java.io.IOException


@AndroidEntryPoint
class DokladActivity : AppCompatActivity(), DokladAdapter.OnItemClickListener {
    private val skladViewModel: SkladViewModel by viewModels()
    var idZebraPref: Int = 0
    var pocetPolozek: Int = 0
    var resultProd: String = ""
    var index: Int = -1

    //private val dokladViewModel: DokladViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        idZebraPref = sharedPreferences.getInt("idZebra",1)


        super.onCreate(savedInstanceState)

        val binding = ActivityDokladBinding.inflate(layoutInflater)
        //setContentView(R.layout.activity_doklad)
        setContentView(binding.root)

        //Toast.makeText(this, "Zebra je $idZebraPref", Toast.LENGTH_LONG).show()

        //Log.d("Doklad", "Zebra : ${idZebraPref.toString()}")
        binding.btBack.setOnClickListener{
            val intent = Intent(this, ButtonActivity::class.java)
            startActivity(intent)
            //super.onBackPressed()
        }
//        binding.btVybratDoklad.setOnClickListener{
//           // Toast.makeText(this, "Kliknul jsi na tlacitko", Toast.LENGTH_LONG ).show()
//
//            openActivity(binding.root)
//            skladViewModel.stav.observe(this@DokladActivity) {
//
//                if (it!=null) {
//
//                    if (it.isNotEmpty()) {
//
//                        AlertDialog.Builder(this@DokladActivity)
//                            .setTitle("Do you wish to continue?")
//                            .setPositiveButton(
//                                R.string.yes,
//                                DialogInterface.OnClickListener { dialog, which ->
//                                    updStavDatabaze()
//                                    // Continue with delete operation
//                                }) // A null listener allows the button to dismiss the dialog and take no further action.
//                            .setNegativeButton(R.string.no, null)
//                            .setIcon(R.drawable.ic_dialog_alert)
//                            .show()
//                    }
//                }
//            }
//
//        }
        //val skladAdapter = SkladAdapter (this)
        val dokladAdapter = DokladAdapter(this, this)
        binding.apply {
//            recyclerViewSklady.apply {
//                adapter = skladAdapter
//                layoutManager = LinearLayoutManager(this@DokladActivity)
//            }
//            skladViewModel.skladyPolozky.observe(this@DokladActivity) {
//                skladAdapter.submitList(it)
//                Log.d("Doklad", skladAdapter.currentList.toString())
//            }

            recyclerViewDoklady.apply {
                adapter = dokladAdapter
                layoutManager = LinearLayoutManager(this@DokladActivity)
            }
            skladViewModel.getAllHlavickyByZebra(idZebraPref).observe(this@DokladActivity){
                dokladAdapter.submitList(it)
                Log.d("Doklad", "Polozky ${dokladAdapter.currentList.toString()}")

            }
        }
    }

    fun openActivity(view: View){
        val intent = Intent(this,PolozkaActivity::class.java )
        startActivity(intent)
    }


    override fun onItemClick(polozkaHlavicka: DokladTuple) {
        resultProd = skladViewModel.getNameProdejna(polozkaHlavicka.odj.toString())
        pocetPolozek = skladViewModel.countPocetPolozekDoklad(polozkaHlavicka.doklad.toString(), polozkaHlavicka.exp.toString())
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        sharedPreferences.edit().putInt("maxSize", pocetPolozek).apply()
        sharedPreferences.edit().putInt("index", index).apply()
        val cisloOdj = polozkaHlavicka.odj.toString()
        var resToShow = ""

        if (resultProd.isNullOrEmpty()){
            resToShow = "Žádná prodejna nebyla nalezena"
        } else {
            resToShow = "Název prodejny je: $resultProd"
                .plus(System.getProperty("line.separator"))
                .plus("Počet řádků: $pocetPolozek" )
        }

        textView2.text = resToShow



       // Toast.makeText(this, "Exp je ${polozkaHlavicka.exp}", Toast.LENGTH_LONG).show()



    }

    override fun onLongClick(polozkaHlavicka: DokladTuple) {
        Log.d("dokladi", "Vkladame doklad")
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        sharedPreferences.edit().putInt("maxSize", pocetPolozek).apply()
        val intent = Intent(this, PolozkaActivity::class.java)
        intent.putExtra("doklad", polozkaHlavicka.doklad)
        intent.putExtra("idExp", polozkaHlavicka.exp)
        Log.d("dokladI",polozkaHlavicka.doklad.toString())
        Log.d("dokladI",polozkaHlavicka.exp.toString())

        //intent.putExtra("pocetPolozek", pocetPolozek)
        intent.putExtra("resultProd", resultProd)
        //intent.putExtra("polozkaHlavicka", polozkaHlavicka as Serializable)
        startActivity(intent)
    }


    fun updStavDatabaze(){
        val url = "${BASE_URL}updStavDoklad?doklad=:doklad&exp=:exp\n" +
                "&sklad=:sklad\n"
        //Toast.makeText(this, url, Toast.LENGTH_SHORT).show()
        try {
            val request = okhttp3.Request.Builder().url(url).build()
            val client = OkHttpClient()
            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Handler(Looper.getMainLooper()).post {
                        Toast.makeText(
                            this@DokladActivity,
                            "Nejaky problem s pripojenim: ${e.message}",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    Log.d("GET", e.message.toString())

                }
                override fun onResponse(call: Call, response: Response) {
                    val body = response.body?.string()
                    Handler(Looper.getMainLooper()).post {
                        Toast.makeText(this@DokladActivity, body, Toast.LENGTH_SHORT).show()
                    }
                }
            })
        } catch (e: IOException) {
            Handler(Looper.getMainLooper()).post {
                Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
            }
        }
    }
}