package com.pors.coopreaderlast.features.doklad

import android.content.Context
import com.pors.coopreaderlast.data.SybaseRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import android.util.Log
import androidx.lifecycle.*
import com.pors.coopreaderlast.data.DokladTuple
import com.pors.coopreaderlast.data.SkladTuple
import dagger.assisted.Assisted
import kotlinx.coroutines.flow.Flow

@HiltViewModel
class SkladViewModel @Inject constructor(
    private val repository: SybaseRepository
): ViewModel(){

    //val zebra: Int = 0
    val skladyPolozky = repository.getAllSkladFromPolozka().asLiveData()
    //val dokladyPolozky = repository.getAllHlavickyToDoklad(zebra).asLiveData()
    val stav = repository.getStav("","").asLiveData()

    //fun dokladyPolozkyGet(idZebra: Int): LiveData<List<DokladTuple>> {
    //    return repository.getAllHlavickyToDoklad(idZebra).asLiveData()
    //}

    fun getAllHlavickyByZebra(zebra: Int): LiveData<List<DokladTuple>>{
        return repository.getAllHlavickyByZebra(zebra).asLiveData()
    }

    fun getNameProdejna(odj: String): String{
        return repository.getNameProdejna(odj)
    }
    fun countPocetPolozekDoklad(doklad: String, exp: String): Int{
        return repository.countPocetPolozekDoklad(doklad, exp)
    }


}

