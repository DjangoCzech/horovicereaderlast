package com.pors.coopreaderlast.features.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.pors.coopreaderlast.data.Skladnik
import com.pors.coopreaderlast.data.SybaseRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import hilt_aggregated_deps._dagger_hilt_android_internal_managers_HiltWrapper_ActivityRetainedComponentManager_ActivityRetainedLifecycleEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val repository: SybaseRepository
):ViewModel() {
    val skladnici = repository.getSkladnik().asLiveData()
    //val hlavicky = repository.getHlavicky().asLiveData()
    //val polozky = repository.getPolozky().asLiveData()
    //val resultLogin = repository.loadAllByIdsAndPwd(id= String(), password = String())

    fun resultLogin(id: String, password:String):Skladnik? {
        return repository.loadAllByIdsAndPwd(id, password)
    }





    //var myResponse: MutableLiveData<Response<SkladnikTest>> = MutableLiveData()
    //val zbozi = repository.getZbozi().asLiveData()

//    fun pushPost(post: SkladnikTest){
//        viewModelScope.launch {
//            val repository: RepositoryPost? = null
//            val response = repository?.pushPost(post)
//            myResponse.value = response
//        }
//
//    }




}