package com.pors.coopreaderlast.features.zebraID

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.activity.viewModels
import androidx.core.view.isVisible
import com.pors.coopreaderlast.R
import com.pors.coopreaderlast.data.Skladnik
import com.pors.coopreaderlast.databinding.ActivityLoginBinding
import com.pors.coopreaderlast.databinding.ActivityZebraBinding
import com.pors.coopreaderlast.features.button.ButtonActivity
import com.pors.coopreaderlast.features.login.LoginActivity
import com.pors.coopreaderlast.features.login.LoginViewModel
import com.pors.coopreaderlast.util.Resource
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_zebra.*

@AndroidEntryPoint
class ZebraActivity : AppCompatActivity() {
    //private val viewModel : ZebraViewModel by viewModels()
    var idZebra : Int = 0
    var idExp: Int = 0
    private val zebraViewModel: ZebraViewModel by viewModels()
    private val spinner3 by lazy {findViewById<Spinner>(R.id.zebra_spinner)}
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityZebraBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        //setContentView(R.layout.activity_zebra)
        Log.d("Zebra", "neco")

        binding.apply {

            zebraViewModel.skladnici.observe(this@ZebraActivity) { result ->
                val skladnici: List<Skladnik>? = result.data
                if (skladnici != null) {
                    Log.e("Skladnici", skladnici.size.toString())
                }
                else{
                    Log.e("Skladnici", "NULL")
                }
                progressBarLogin.isVisible =
                    result is Resource.Loading && result.data.isNullOrEmpty()
                textViewProgressBar.isVisible =
                    result is Resource.Loading && result.data.isNullOrEmpty()
                textViewProgressBar.text = "Nahrávám data z databáze do čtečky"
                textViewErrorLogin.isVisible =
                    result is Resource.Error && result.data.isNullOrEmpty()
                textViewErrorLogin.text = result.error?.localizedMessage
                textViewErrorLogin.isVisible = result is Resource.Success
                textViewErrorLogin.text = "Data nahrána"

            }
        }

        //initSpinnerData()
        bt_OK.setOnClickListener{
            //Toast.makeText(this@ZebraActivity, "Číslo Zebry je : $idZebra}", Toast.LENGTH_LONG).show()
            val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
            sharedPreferences.edit().putInt("idZebra", idZebra).apply()
            sharedPreferences.edit().putInt("idExp", idExp ).apply()
            startActivity(Intent(this, LoginActivity::class.java))

        }


    }

    override fun onResume() {
        super.onResume()
        initSpinnerData()
    }

    private fun initSpinnerData(){

        val allDevices = this?.let {
            ArrayAdapter<Any>(it, R.layout.simple_item_spinner, R.id.tv_simple_item)
        }
        //vymaze vsechny polozky v seznamu
        //vyzkouset
        allDevices.clear()
        zebraViewModel.getAllZebra()
            .observe(this, { devices ->
                devices?.forEach{
                    if (allDevices.getPosition(it) < 0) allDevices?.add(it)
                }
            } )
        spinner3.adapter = allDevices
        spinner3.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                if (parent != null) {
                    val idZebraString = parent.getItemAtPosition(position).toString()
                    idZebra = idZebraString.toInt()

                }
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                Toast.makeText(this@ZebraActivity, "Musite si vybrat nejakou polozku", Toast.LENGTH_LONG).show()            }

        }
    }
}