package com.pors.coopreaderlast.features.informationDW
import androidx.lifecycle.ViewModel
import com.pors.coopreaderlast.data.SybaseRepository
import com.pors.coopreaderlast.data.Zbozi
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class InfoActivityDWViewModel@Inject constructor(
    private val repository: SybaseRepository
): ViewModel() {
    //val infoByEan = repository.getInfoByEAN(ean).asLiveData()

    fun infoByEan(ean: String): Zbozi {
        return repository.getInfoByEAN(ean)
    }
    fun getCountRegByEAN(ean:String):Int{
        return repository.getCountRegByEAN(ean)
    }
}