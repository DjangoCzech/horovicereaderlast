package com.pors.coopreaderlast.features.polozka

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.pors.coopreaderlast.data.DokladTuple
import com.pors.coopreaderlast.data.Polozka
import com.pors.coopreaderlast.data.PolozkaTuple
import com.pors.coopreaderlast.databinding.PolozkyItemBinding

class PolozkaAdapter(val chosen_item: Int, private val listener: OnItemClickListener): ListAdapter<Polozka, PolozkaAdapter.PolozkaViewHolder>(DiffCallback()){
    //val chosen_item: Int,
    var selectedItemPosition: Int = chosen_item
    //val itemToChange = chosen_item
    //lateinit var viewHolder: PolozkaViewHolder

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PolozkaViewHolder {
        val binding = PolozkyItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PolozkaViewHolder(binding)
       // viewHolder = PolozkaViewHolder(binding)
        //return viewHolder
    }
    override fun onBindViewHolder(holder: PolozkaViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
//        holder.itemView.setOnClickListener {
//            selectedItemPosition = position
//            notifyDataSetChanged()
//        }
//        if (currentItem.selected){
//            holder.itemView.setBackgroundColor(Color.parseColor("#DA745A"))
//        } else
//        {
//            holder.itemView.setBackgroundColor(Color.TRANSPARENT)
//        }

        if (selectedItemPosition == position){
            holder.itemView.setBackgroundColor(Color.parseColor("#DA745A"))
        } else
        {
            holder.itemView.setBackgroundColor(Color.TRANSPARENT)
        }
//        if (selectedItemPosition == position)
//            holder.itemView.setBackgroundColor(Color.parseColor("#DC746C"))
//        else
//            holder.itemView.setBackgroundColor(Color.parseColor("#FFFFFF"))


    }
    //fun getCurrentItem(): Polozka? = currentList.find { it.selected }

    //fun getCurrentItem(index:Int): String? = currentList

    override fun getItemId(position: Int): Long {
        return super.getItemId(position)
    }
//    fun getCurrentItem():Polozka{
//        return super.getItem()
//    }

    override fun getItemCount(): Int {
        return super.getItemCount()
    }

    inner class PolozkaViewHolder(private val binding: PolozkyItemBinding): RecyclerView.ViewHolder(binding.root){
        //var currentIndex = 0
        init {
            binding.root.setOnClickListener{
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION){
                    val item = getItem(position)
                    if (item != null){
                        listener.onItemClick(item, position)
                        //listener.onItemClickListener(item, position)
                    }
                }
                notifyItemChanged(selectedItemPosition)
                //selectedItemPosition = position
                selectedItemPosition = bindingAdapterPosition
                notifyItemChanged(selectedItemPosition)
            }
            binding.root.setOnLongClickListener{
                val positionLong = bindingAdapterPosition
                if (positionLong != RecyclerView.NO_POSITION){
                    val itemLong = getItem(positionLong)
                    if(itemLong != null){
                        listener.onLongClick(itemLong, positionLong)
                    }
                }
                true
            }
        }

        fun bind(polozkaPolozka: Polozka){
            binding.apply {

                //tvStar.text =  if //(polozkaPolozka.mnoz_vyd == 0.toFloat()){
                    //""
                tvStar.text = if(polozkaPolozka.stav == 1){
                    ""
                }else (if (polozkaPolozka.mnoz_vyd != polozkaPolozka.mnoz_obj && polozkaPolozka.stav == 2){
                                "-"
                            } else if (polozkaPolozka.mnoz_vyd == polozkaPolozka.mnoz_obj && polozkaPolozka.stav == 2){
                                "*"
                            } else{
                                ""
                            })
//
                tvKDE.text = polozkaPolozka.znacky
                tvREG.text = polozkaPolozka.reg
                tvVB.text = polozkaPolozka.veb.toString()
                tvMN.text = polozkaPolozka.mnoz_obj.toString()
                tvMNV.text = polozkaPolozka.mnoz_vyd.toString()
                tvDATSPOTR.text = polozkaPolozka.datspo
                tvSARZE.text = polozkaPolozka.sarze

            }
        }
        fun changeStar(item: Int): String {


            if (item == 2){
                return "*"
            } else {
                return ""
            }
        }
    }
    interface OnItemClickListener{
        fun onItemClick(polozkaDoklad: Polozka, position: Int)
        fun onLongClick(polozkaDoklad: Polozka, position: Int)
        //fun onItemClickListener(item: Polozka, position: Int)

    }


    class DiffCallback: DiffUtil.ItemCallback<Polozka>(){
        override fun areItemsTheSame(oldItem: Polozka, newItem: Polozka) =
            oldItem.pvp06pk == newItem.pvp06pk


        override fun areContentsTheSame(oldItem: Polozka, newItem: Polozka) =
            oldItem == newItem

    }
}