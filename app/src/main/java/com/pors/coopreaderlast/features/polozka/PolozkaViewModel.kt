package com.pors.coopreaderlast.features.polozka

import android.os.Parcelable
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.pors.coopreaderlast.data.Polozka
import com.pors.coopreaderlast.data.PolozkaTuple
import com.pors.coopreaderlast.data.SybaseRepository
import dagger.Provides
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import javax.inject.Named

@HiltViewModel
class PolozkaViewModel @Inject constructor(
    //@Named("zebra") var zebra: String,
    //@Named("doklad") var doklad: String,
    repository: SybaseRepository
):
    ViewModel(){
    var repo = repository
    private lateinit var state: Parcelable
    fun saveRecyclerViewState(parcelable: Parcelable) { state = parcelable }
    fun restoreRecyclerViewState() : Parcelable = state
    fun stateInitialized() : Boolean = ::state.isInitialized

    fun getall(doklad: String, exp: String): LiveData<List<Polozka>> {
        return repo.getAllPolozkyByDoklad(doklad, exp).asLiveData()
    }

    fun getAllPolozkyByDokladNesouhlasiMnozstvi(doklad: String, exp: String): LiveData<List<Polozka>> {
        return repo.getAllPolozkyByDokladNesouhlasiMnozstvi(doklad, exp).asLiveData()
    }
    fun getAllPolozkyByDokladSplnene(doklad: String, exp: String): LiveData<List<Polozka>> {
        return repo.getAllPolozkyByDokladSplnene(doklad, exp).asLiveData()
    }
    fun getAllPolozkyByDokladNezpracovane(doklad: String, exp: String): LiveData<List<Polozka>> {
        return repo.getAllPolozkyByDokladNezpracovane(doklad, exp).asLiveData()
    }
    fun getCountNaskladnenePolozky(doklad: String, exp: String, stav: Int, reg:String): Int{
        return repo.getCountNaskladnenePolozky(doklad, exp, stav, reg)
    }

    fun getInfoByREG(reg: String):String{
        return repo.getInfoByREG(reg)
    }

    fun getRegByEAN(ean: String): String{
        return repo.getRegByEAN(ean)
    }
    fun getCountRegByEAN(ean:String):Int{
        return repo.getCountRegByEAN(ean)
    }
    fun getCountRegDoklad(reg: String, doklad: String): Int{
        return repo.getCountRegDoklad(reg, doklad)
    }

    fun loadAllPolozkaByReg(reg: String, doklad: String): Polozka{
        return repo.loadAllPolozkaByReg(reg, doklad)
    }

    fun countStavZebrap(doklad: String,exp: String):Int{
        return repo.countStavZebrap(doklad, exp)
    }
    fun countPocetZebrap(reg: String, doklad: String): Int{
        return repo.countPocetZebrap(reg, doklad)
    }
    fun updStavDokladUkonceniZ(doklad: String){
        return repo.updStavDokladUkonceniZ(doklad)
    }
    fun updStavDokladUkonceniP(doklad: String){
        return repo.updStavDokladUkonceniP(doklad)
    }

}