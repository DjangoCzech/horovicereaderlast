package com.pors.coopreaderlast.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cis06zebraz", primaryKeys = ["doklad", "exp", "zebra"])
data class Hlavicka(
    val doklad: String,
    val org: String? = null,
    val odj: String? = null,
    val datuct: String? = null,
    val rozvoz: String? = null,
    val stav: Int? = null,
    val pocpol: String? = null,
    val prac: String? = null,
    val zebra: String,
    val exp: String
)
