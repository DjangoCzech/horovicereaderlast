package com.pors.coopreaderlast.data

import androidx.room.ColumnInfo
import java.io.Serializable

data class PolozkaTuple (
    @ColumnInfo (name = "reg") val reg: String?,
    @ColumnInfo (name = "znacky") val znacky: String?,
    @ColumnInfo (name = "mnoz_obj") val mnoz_obj: String?,
    @ColumnInfo (name = "mnoz_vyd") val mnoz_vyd: String?,
    @ColumnInfo (name = "datspo") val datspo: String?,
    @ColumnInfo (name = "sarze") val sarze: String?,
    @ColumnInfo (name = "stav") val stav: Int?
): Serializable
