package com.pors.coopreaderlast.data

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [Skladnik::class, Polozka::class, Hlavicka::class, Zbozi::class, Prodejna::class], version = 1, exportSchema = false)
abstract class SybaseDatabase : RoomDatabase() {
    abstract fun sybaseDao(): SybaseDao
}