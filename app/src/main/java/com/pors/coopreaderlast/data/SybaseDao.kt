package com.pors.coopreaderlast.data

import androidx.lifecycle.LiveData
import androidx.room.*
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.flow.Flow
import retrofit2.Response
@SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
@Dao
interface SybaseDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSkladnik(skladnik: List<Skladnik>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertHlavicka(hlavicka: List<Hlavicka>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPolozka(polozka: List<Polozka>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertZbozi(zbozi: List<Zbozi>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProdejna(prodejna: List<Prodejna>)

    @Query("DELETE FROM cis06zebras")
    suspend fun deleteAllSkladnici()

    @Query("DELETE FROM cis06zebrap")
    suspend fun deleteAllPolozky()

    @Query("DELETE FROM cis06zebraz")
    suspend fun deleteAllHlavicky()

    @Query("DELETE FROM cis06zebrar")
    suspend fun deleteAllZbozi()

    @Query("DELETE FROM cis06zebrao")
    suspend fun deleteAllProdejna()

    @Query("SELECT * FROM cis06zebras")
    fun getAllSkladnici(): Flow<List<Skladnik>>

    @Query("SELECT * FROM cis06zebrao")
    fun getAllProdejna(): Flow<List<Prodejna>>

    @Query("SELECT * FROM cis06zebraz")
    fun getAllHlavicky(): Flow<List<Hlavicka>>

    @Query("SELECT stav, doklad, odj, datuct, exp FROM cis06zebraz WHERE zebra = :zebra AND stav = 1")
    fun getAllHlavickyByZebra(zebra: Int): Flow<List<DokladTuple>>

    @Query("SELECT * FROM cis06zebraz")
    fun getAllHlavickyList(): Hlavicka?

    @Query("SELECT * FROM cis06zebrap")
    fun getAllPolozky(): Flow<List<Polozka>>

    @Query("SELECT * FROM cis06zebrar")
    fun getAllZbozi(): Flow<List<Zbozi>>

    @Query("SELECT distinct sklad FROM cis06zebrap")
    fun getAllSkladFromPolozka(): Flow<List<SkladTuple>>

    @Query("SELECT distinct sklad,doklad FROM cis06zebrap ORDER BY sklad")
    fun getAllSkladDokladFromPolozkaBySklad(): Flow<List<SkladDokladTuple>>

    @Query("SELECT distinct doklad FROM cis06zebrap where sklad=:skladId")
    fun getAllDokladFromPolozkaBySklad(skladId:Int?=null): Flow<List<SkladDokladTuple>>

    @Query("""select * from cis06zebrap 
            where doklad = :doklad and exp = :exp order by sklad, znacky, reg""")
    fun getAllPolozkyByDoklad(doklad: String, exp: String): Flow<List<Polozka>>

    @Query("""select COUNT() from cis06zebrap 
            where doklad = :doklad and exp = :exp and stav = :stav and reg = :reg and mnoz_obj = mnoz_vyd""")
    fun getCountNaskladnenePolozky(doklad: String, exp: String, stav: Int, reg:String): Int

    @Query("""select * from cis06zebrap 
            where doklad = :doklad and exp = :exp and mnoz_vyd <> mnoz_obj order by znacky, sklad, reg""")
    fun getAllPolozkyByDokladNesouhlasiMnozstvi(doklad: String, exp: String): Flow<List<Polozka>>

    @Query("""select * from cis06zebrap 
            where doklad = :doklad and exp = :exp and stav = 2 order by znacky, sklad, reg""")
    fun getAllPolozkyByDokladSplnene(doklad: String, exp: String): Flow<List<Polozka>>

    @Query("""select * from cis06zebrap 
            where doklad = :doklad and exp = :exp and stav <> 2 order by znacky, sklad, reg""")
    fun getAllPolozkyByDokladNezpracovane(doklad: String, exp: String): Flow<List<Polozka>>


    //@Query("SELECT stav, doklad, odj, datuct FROM cis06zebraz WHERE zebra = :zebra AND stav = 1")
    //getAllHlavickyByZebrafun getAllHlavickyToDoklad(zebra: Int): Flow<List<DokladTuple>>

    //@Query("SELECT stav, doklad, odj, datuct FROM cis06zebraz")
    //fun getAllHlavickyToDokladTest(): Flow<List<DokladTuple>>

    @Query("SELECT cak, reg, naz, znacky, zasoba, veb FROM cis06zebrar WHERE cak=:ean")
    fun getInfoByEAN(ean:String):Zbozi

    @Query("SELECT naz FROM cis06zebrar WHERE reg=:reg")
    fun getInfoByREG(reg:String):String

    @Query("SELECT naz FROM cis06zebrar WHERE reg=:reg LIMIT 1")
    fun getEANByREG(reg:String):String

    @Query("SELECT reg FROM cis06zebrar WHERE cak=:ean")
    fun getRegByEAN(ean:String):String

    @Query("SELECT COUNT() FROM cis06zebrar WHERE cak=:ean")
    fun getCountRegByEAN(ean:String):Int

    //Nasledujici dva updaty se provedou pri stazeni dat do zebry
    @Query("update cis06zebrap set stav = 1 where stav = 0")
    fun updStavyZebraPStavPolozka()
    @Query("update cis06zebraz set stav = 1, prac = :prac where cis06zebraz.stav = 0 and zebra = :zebra")
    fun updStavyZebraZStavPolozka(prac: Int, zebra: Int)

    //Update mnozstvi
    @Query("update cis06zebrap set mnoz_vyd = :mnoz_vyd, prac = :prac, stav = 2 where pvp06pk = :pvp06pk")
    fun updStavMnozstviPolozky(mnoz_vyd: Int, prac: String, pvp06pk: String)

    //Nasledujici dva updaty se provedou pri uzavreni dokladu
    @Query("update cis06zebraz set stav = 2 where doklad = :doklad")
    fun updStavDokladUkonceniZ(doklad: String)
    @Query("update cis06zebrap set stav = 2 where doklad = :doklad")
    fun updStavDokladUkonceniP(doklad: String)



    //@Transaction @Query("UPDATE cis06zebraz, cis06zebrap set cis06zebraz.stav = 1,cis06zebrap.stav=1 where cis06zebraz.doklad=cis06zebrap.doklad and cis06zebraz.exp = cis06zebrap.exp and cis06zebraz.stav = 0 and cis06zebraz.zebra = :zebra")
    //fun updStavDatabaze(zebra:String)

    @Query("update cis06zebrap set mnoz_vyd = :mnoz_vyd, prac=:prac, stav = 2 where pvp06pk = :pvp06pk")
    fun updStavPolozka(mnoz_vyd: String, prac: String, pvp06pk:String)

    @Query("select stav from cis06zebrap where doklad = :doklad and exp = :exp and stav = 1")
    fun getStav(doklad: String, exp:String):Flow<List<PolozkaStavTuple>>

    @Query("update cis06zebrap set stav=2 where doklad = :doklad and exp = :exp and sklad = :sklad")
    fun updStavDoklad(doklad: String, exp: String, sklad:String)

    @Query("SELECT * FROM cis06zebras WHERE id = :id AND heslo = :heslo")
    fun loadAllByIdsAndPwd(id: String, heslo: String ): Skladnik?

    @Query("SELECT * FROM cis06zebras")
    fun loadAllByIdsAndPwd(): List<Skladnik>?

    @Query("SELECT * FROM cis06zebrap WHERE reg = :reg AND doklad = :doklad")
    fun loadAllPolozkaByReg(reg: String, doklad: String): Polozka

    @Query("SELECT COUNT() FROM cis06zebrap WHERE reg = :reg AND doklad = :doklad")
    fun getCountRegDoklad(reg: String, doklad: String): Int

    @Query("SELECT zebra FROM cis06zebras ORDER BY zebra")
    fun getAllZebra(): LiveData<List<Int>>

    @Query("select count(stav) from cis06zebrap where doklad = :doklad and exp = :exp and stav = 1")
    fun countStavZebrap(doklad: String, exp: String): Int

    @Query("select count() from cis06zebrap where reg = :reg and doklad = :doklad")
    fun countPocetZebrap(reg: String, doklad: String): Int
    @Query("SELECT COUNT() FROM cis06zebrap WHERE doklad = :doklad AND exp = :exp")
    fun countPocetPolozekDoklad(doklad: String, exp: String): Int

    @Query("SELECT naz FROM cis06zebrao WHERE odj = :odj")
    fun getNameProdejna(odj: String): String
}