package com.pors.coopreaderlast.data

import androidx.datastore.preferences.core.emptyPreferences
import androidx.lifecycle.LiveData
import androidx.room.withTransaction
import com.pors.coopreaderlast.api.SybaseApi
import com.pors.coopreaderlast.util.networkBoundResource
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class SybaseRepository @Inject constructor(
    private val api: SybaseApi,
    private val db: SybaseDatabase
) {
    private val sybaseDao = db.sybaseDao()

    fun getSkladnik() = networkBoundResource(
        query = {
            sybaseDao.getAllSkladnici()
        },
        fetch = {
            api.getSkladnik()
        },
        saveFetchResult = { skladnici ->
            db.withTransaction {
                //sybaseDao.deleteAllSkladnici()
                sybaseDao.insertSkladnik(skladnici)
            }

        }
    )

    fun getHlavicky() = networkBoundResource(
        query = {
            sybaseDao.getAllHlavicky()
        },
        fetch = {
            api.getHlavicky()
        },
        saveFetchResult = { hlavicky ->
            db.withTransaction {
                //sybaseDao.deleteAllHlavicky()
                sybaseDao.insertHlavicka(hlavicky)
            }

        }
    )

    fun getPolozky() = networkBoundResource(
        query = {
            sybaseDao.getAllPolozky()
        },
        fetch = {
            api.getPolozky()
        },
        saveFetchResult = { polozky ->
            db.withTransaction {
                //sybaseDao.deleteAllPolozky()
                sybaseDao.insertPolozka(polozky)
            }

        }
    )

    fun getZbozi() = networkBoundResource(
        query = {
            sybaseDao.getAllZbozi()
        },
        fetch = {
            api.getZbozi()
        },
        saveFetchResult = { zbozi ->
            db.withTransaction {
                sybaseDao.deleteAllZbozi()
                sybaseDao.insertZbozi(zbozi)
            }

        }
    )

    fun getProdejna() = networkBoundResource(
        query = {
            sybaseDao.getAllProdejna()
        },
        fetch = {
            api.getProdejna()
        },
        saveFetchResult = { prodejna ->
            db.withTransaction {
                sybaseDao.deleteAllProdejna()
                sybaseDao.insertProdejna(prodejna)
            }
        }
    )

    fun getAllHlavicky(): Flow<List<Hlavicka>>{
        return sybaseDao.getAllHlavicky()
    }

    fun getAllSkladFromPolozka(): Flow<List<SkladTuple>> {
        return sybaseDao.getAllSkladFromPolozka()
    }

    fun getCountNaskladnenePolozky(doklad: String, exp: String, stav: Int, reg:String): Int{
        return sybaseDao.getCountNaskladnenePolozky(doklad, exp, stav, reg)
    }

    fun getAllPolozkyByDoklad(doklad: String, exp:String): Flow<List<Polozka>>{
        return sybaseDao.getAllPolozkyByDoklad(doklad, exp)
    }

    fun getAllPolozkyByDokladNesouhlasiMnozstvi(doklad: String, exp:String): Flow<List<Polozka>>{
        return sybaseDao.getAllPolozkyByDokladNesouhlasiMnozstvi(doklad, exp)
    }
    fun getAllPolozkyByDokladSplnene(doklad: String, exp:String): Flow<List<Polozka>>{
        return sybaseDao.getAllPolozkyByDokladSplnene(doklad, exp)
    }
    fun getAllPolozkyByDokladNezpracovane(doklad: String, exp:String): Flow<List<Polozka>>{
        return sybaseDao.getAllPolozkyByDokladNezpracovane(doklad, exp)
    }


    /*fun getAllHlavickyToDokladTest(): Flow<List<DokladTuple>>{
        return sybaseDao.getAllHlavickyToDokladTest()
    }*/

    //fun getAllHlavickyToDoklad(zebra: Int): Flow<List<DokladTuple>>{
    //    return sybaseDao.getAllHlavickyToDoklad(zebra)
    //}

    fun getSelectedDokladyBySklad(sklad: Int) : Flow<List<SkladDokladTuple>>{
        return sybaseDao.getAllDokladFromPolozkaBySklad(sklad)
    }
    fun getInfoByEAN(ean: String): Zbozi{
        return sybaseDao.getInfoByEAN(ean)
    }
    fun getInfoByREG(reg: String): String{
        return sybaseDao.getInfoByREG(reg)
    }
    fun getRegByEAN(ean: String): String{
        return sybaseDao.getRegByEAN(ean)
    }
    fun getCountRegByEAN(ean:String):Int{
        return sybaseDao.getCountRegByEAN(ean)
    }
    fun getEANByREG(reg: String): String{
        return sybaseDao.getEANByREG(reg)
    }
    fun loadAllByIdsAndPwd(id: String, password: String): Skladnik? {
        return sybaseDao.loadAllByIdsAndPwd(id, password)
    }
    fun loadAllByIdsAndPwd(): List<Skladnik>? {
        return sybaseDao.loadAllByIdsAndPwd()
    }

    fun getAllHlavickyByZebra(zebra: Int): Flow<List<DokladTuple>>{
        return sybaseDao.getAllHlavickyByZebra(zebra)
    }

    fun loadAllPolozkaByReg(reg: String, doklad: String): Polozka{
        return sybaseDao.loadAllPolozkaByReg(reg, doklad)
    }

    fun getStav(doklad: String, exp:String): Flow<List<PolozkaStavTuple>> {
        return sybaseDao.getStav(doklad, exp)
    }

    fun getAllZebra(): LiveData<List<Int>>{
        return sybaseDao.getAllZebra()
    }
    fun countStavZebrap(doklad: String,exp: String): Int{
        return sybaseDao.countStavZebrap(doklad, exp)
    }
    fun countPocetZebrap(reg: String, doklad: String): Int{
        return sybaseDao.countPocetZebrap(reg, doklad)
    }

    fun countPocetPolozekDoklad(doklad: String, exp: String): Int{
        return sybaseDao.countPocetPolozekDoklad(doklad, exp)
    }

    fun getCountRegDoklad(reg: String, doklad: String): Int{
        return sybaseDao.getCountRegDoklad(reg, doklad)
    }
    //update pri nacteni databaze
    fun updStavyZebraPStavPolozka(){
        return sybaseDao.updStavyZebraPStavPolozka()
    }
    fun updStavyZebraZStavPolozka(prac: Int, zebra: Int){
        return sybaseDao.updStavyZebraZStavPolozka(prac, zebra)
    }

    fun updStavMnozstviPolozky(mnoz_vyd: Int, prac: String, pvp06pk: String){
        return sybaseDao.updStavMnozstviPolozky(mnoz_vyd, prac, pvp06pk)
    }

    fun updStavDokladUkonceniZ(doklad: String){
        return sybaseDao.updStavDokladUkonceniZ(doklad)
    }
    fun updStavDokladUkonceniP(doklad: String){
        return sybaseDao.updStavDokladUkonceniP(doklad)
    }

    fun getNameProdejna(odj: String):String{
        return sybaseDao.getNameProdejna(odj)
    }



}