package com.pors.coopreaderlast.data

data class SkladnikTest(
    val prac: String,
    val id: String,
    val heslo: String,
    val skladnik: String,
    val zebra: String
)
