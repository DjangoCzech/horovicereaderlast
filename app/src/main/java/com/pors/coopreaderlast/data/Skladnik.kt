package com.pors.coopreaderlast.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cis06zebras")
data class Skladnik(
    @PrimaryKey val prac: String,
    val id: String? = null,
    val heslo: String? = null,
    val skladnik: String? = null,
    val zebra: Int? = null
)
