package com.pors.coopreaderlast.data

import androidx.room.ColumnInfo

data class SkladDokladTuple(
    @ColumnInfo(name = "sklad") val sklad: Int?,
    @ColumnInfo(name = "doklad") val doklad: Int?

)
