package com.pors.coopreaderlast.data

import androidx.room.Entity
import androidx.room.PrimaryKey
//tabulka vseho zbozi, ktere se prodava
//ciselnik zbozi bude slozeny primarni klic
@Entity(tableName = "cis06zebrar")
data class Zbozi(
    @PrimaryKey val cak: String,
    val naz: String? = null,
    val veb: String? = null,
    val reg: String? = null,
    val zasoba: String? = null,
    val znacky: String? = null
)
