package com.pors.coopreaderlast.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "cis06zebrap")
data class Polozka(
    val doklad: String? = null,
    val sklad: String? = null,
    val reg: String? = null,
    val mnoz_obj: Float? = null,
    val mnoz_vyd: Float? = null,
    val kc_pce: Float? = null,
    val sarze: String? = null,
    val datspo: String? = null,
    val veb: Float? = null,
    val poc2: String? = null,
    @PrimaryKey val pvp06pk: String,
    val znacky: String? = null,
    val stav: Int? = null,
    val prac: String? = null,
    val exp: String? = null,
    val selected: Boolean
):Serializable
