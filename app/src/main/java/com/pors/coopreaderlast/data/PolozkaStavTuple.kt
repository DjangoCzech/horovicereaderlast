package com.pors.coopreaderlast.data

import androidx.room.ColumnInfo

data class PolozkaStavTuple(
    @ColumnInfo(name = "stav") val stav: Int?
)