package com.pors.coopreaderlast.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cis06zebrao")
data class Prodejna(
    val org: String? = null,
    @PrimaryKey val odj: String,
    val naz: String? = null
)
