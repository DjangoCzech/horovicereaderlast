package com.pors.coopreaderlast.data

import androidx.room.ColumnInfo
import androidx.room.Ignore
import androidx.room.RoomWarnings

@SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
data class SkladTuple(
    @ColumnInfo(name = "sklad") val sklad: Int?,

    @ColumnInfo(name = "reg") val reg: Int?
)
