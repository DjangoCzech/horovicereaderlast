package com.pors.coopreaderlast.data

import androidx.room.ColumnInfo
import java.io.Serializable

data class HlavickaTuple (
    @ColumnInfo(name = "stav") val stav: String?,
    @ColumnInfo(name = "doklad") val doklad: String?,
    @ColumnInfo(name = "odj") val odj: String?,
    @ColumnInfo(name = "datuct") val datuct: String?,
    @ColumnInfo(name = "exp") val exp: String?
    ):Serializable